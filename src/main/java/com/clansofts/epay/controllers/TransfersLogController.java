package com.clansofts.epay.controllers;

import com.clansofts.epay.entities.TransfersLog;
import com.clansofts.epay.controllers.util.ClansoftsUtil;
import com.clansofts.epay.controllers.util.ClansoftsUtil.PersistAction;
import com.clansofts.epay.facades.TransfersLogFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("transfersLogController")
@SessionScoped
public class TransfersLogController implements Serializable {

    @EJB
    private com.clansofts.epay.facades.TransfersLogFacade ejbFacade;
    private List<TransfersLog> items = null;
    private TransfersLog selected;

    public TransfersLogController() {
    }

    public TransfersLog getSelected() {
        return selected;
    }

    public void setSelected(TransfersLog selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private TransfersLogFacade getFacade() {
        return ejbFacade;
    }

    public TransfersLog prepareCreate() {
        selected = new TransfersLog();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("TransfersLogCreated"));
        if (!ClansoftsUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("TransfersLogUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("TransfersLogDeleted"));
        if (!ClansoftsUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<TransfersLog> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                ClansoftsUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    ClansoftsUtil.addErrorMessage(msg);
                } else {
                    ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
            }
        }
    }

    public TransfersLog getTransfersLog(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<TransfersLog> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<TransfersLog> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = TransfersLog.class)
    public static class TransfersLogControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TransfersLogController controller = (TransfersLogController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "transfersLogController");
            return controller.getTransfersLog(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TransfersLog) {
                TransfersLog o = (TransfersLog) object;
                return getStringKey(o.getTransfersLogID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), TransfersLog.class.getName()});
                return null;
            }
        }

    }

}
