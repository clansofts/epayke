package com.clansofts.epay.controllers;

import com.clansofts.epay.entities.OrganizationStatus;
import com.clansofts.epay.controllers.util.ClansoftsUtil;
import com.clansofts.epay.controllers.util.ClansoftsUtil.PersistAction;
import com.clansofts.epay.facades.OrganizationStatusFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("organizationStatusController")
@SessionScoped
public class OrganizationStatusController implements Serializable {

    @EJB
    private com.clansofts.epay.facades.OrganizationStatusFacade ejbFacade;
    private List<OrganizationStatus> items = null;
    private OrganizationStatus selected;

    public OrganizationStatusController() {
    }

    public OrganizationStatus getSelected() {
        return selected;
    }

    public void setSelected(OrganizationStatus selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private OrganizationStatusFacade getFacade() {
        return ejbFacade;
    }

    public OrganizationStatus prepareCreate() {
        selected = new OrganizationStatus();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("OrganizationStatusCreated"));
        if (!ClansoftsUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("OrganizationStatusUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("OrganizationStatusDeleted"));
        if (!ClansoftsUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<OrganizationStatus> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                ClansoftsUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    ClansoftsUtil.addErrorMessage(msg);
                } else {
                    ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
            }
        }
    }

    public OrganizationStatus getOrganizationStatus(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<OrganizationStatus> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<OrganizationStatus> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = OrganizationStatus.class)
    public static class OrganizationStatusControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            OrganizationStatusController controller = (OrganizationStatusController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "organizationStatusController");
            return controller.getOrganizationStatus(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof OrganizationStatus) {
                OrganizationStatus o = (OrganizationStatus) object;
                return getStringKey(o.getOrganizationStatusID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), OrganizationStatus.class.getName()});
                return null;
            }
        }

    }

}
