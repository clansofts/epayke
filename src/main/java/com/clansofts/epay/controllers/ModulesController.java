package com.clansofts.epay.controllers;

import com.clansofts.epay.entities.Modules;
import com.clansofts.epay.controllers.util.ClansoftsUtil;
import com.clansofts.epay.controllers.util.ClansoftsUtil.PersistAction;
import com.clansofts.epay.facades.ModulesFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("modulesController")
@SessionScoped
public class ModulesController implements Serializable {

    @EJB
    private com.clansofts.epay.facades.ModulesFacade ejbFacade;
    private List<Modules> items = null;
    private Modules selected;

    public ModulesController() {
    }

    public Modules getSelected() {
        return selected;
    }

    public void setSelected(Modules selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ModulesFacade getFacade() {
        return ejbFacade;
    }

    public Modules prepareCreate() {
        selected = new Modules();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("ModulesCreated"));
        if (!ClansoftsUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("ModulesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("ModulesDeleted"));
        if (!ClansoftsUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Modules> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    public List<Modules> getaLLItems() {
        return this.ejbFacade.findAll();
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                ClansoftsUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    ClansoftsUtil.addErrorMessage(msg);
                } else {
                    ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Modules getModules(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Modules> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Modules> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Modules.class)
    public static class ModulesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ModulesController controller = (ModulesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "modulesController");
            return controller.getModules(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Modules) {
                Modules o = (Modules) object;
                return getStringKey(o.getModuleID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Modules.class.getName()});
                return null;
            }
        }

    }

}
