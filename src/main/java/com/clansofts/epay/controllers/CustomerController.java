package com.clansofts.epay.controllers;

import com.clansofts.epay.entities.Customer;
import com.clansofts.epay.controllers.util.ClansoftsUtil;
import com.clansofts.epay.controllers.util.ClansoftsUtil.PersistAction;
import com.clansofts.epay.facades.CustomerFacade;
import com.clansofts.pf.support.libs.AgrilifeMessagesSupport;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("customerController")
@SessionScoped
public class CustomerController implements Serializable {

    @EJB
    private com.clansofts.epay.facades.CustomerFacade ejbFacade;
    private List<Customer> items = null;
    private Customer selected;
    private  Customer customer;

    public CustomerController() {
        customer = new Customer();
    }

    public Customer getSelected() {
        return selected;
    }

    public void setSelected(Customer selected) {
        this.selected = selected;
    }

    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private CustomerFacade getFacade() {
        return ejbFacade;
    }

    public Customer prepareCreate() {
        selected = new Customer();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("CustomerCreated"));
        if (!ClansoftsUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("CustomerUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("CustomerDeleted"));
        if (!ClansoftsUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Customer> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                ClansoftsUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    ClansoftsUtil.addErrorMessage(msg);
                } else {
                    ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
            }
        }
    }
    
    public String selfRegister() {
        String url = "Boom";
        
//        customPersist(PersistAction.CREATE, 
//                ResourceBundle.
//                        getBundle("com/clansofts/epay/configs").
//                        getString("CustomerCreated"));
        
         //Success
         AgrilifeMessagesSupport.getOutputInfoMessage("Customer Setup Successful", "Customer successfully created");

        
//        if (!ClansoftsUtil.isValidationFailed()) {
//            items = null;    // Invalidate list of items to trigger re-query.
//        }
        
        return url;
    }

    public void customUpdate() {
        customPersist(PersistAction.UPDATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("CustomerUpdated"));
    }
    
    private void customPersist(PersistAction persistAction, String successMessage) {
        if (customer != null) {
            setEmbeddableKeys();
            try {
                
                
                if (persistAction == PersistAction.UPDATE) {
                    getFacade().edit(customer);
                } else if (PersistAction.CREATE == persistAction) {
                    customer.setCustomerID(this.getFacade().getNextEntityDataId());
                    getFacade().create(customer);
                }else {
                    getFacade().remove(customer);
                }
                ClansoftsUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    ClansoftsUtil.addErrorMessage(msg);
                } else {
                    ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Customer getCustomer(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Customer> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Customer> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Customer.class)
    public static class CustomerControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CustomerController controller = (CustomerController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "customerController");
            return controller.getCustomer(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Customer) {
                Customer o = (Customer) object;
                return getStringKey(o.getCustomerID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Customer.class.getName()});
                return null;
            }
        }

    }

}
