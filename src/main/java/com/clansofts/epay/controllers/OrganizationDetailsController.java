package com.clansofts.epay.controllers;

import com.clansofts.epay.entities.OrganizationDetails;
import com.clansofts.epay.controllers.util.ClansoftsUtil;
import com.clansofts.epay.controllers.util.ClansoftsUtil.PersistAction;
import com.clansofts.epay.facades.OrganizationDetailsFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("organizationDetailsController")
@SessionScoped
public class OrganizationDetailsController implements Serializable {

    @EJB
    private com.clansofts.epay.facades.OrganizationDetailsFacade ejbFacade;
    private List<OrganizationDetails> items = null;
    private OrganizationDetails selected;

    public OrganizationDetailsController() {
    }

    public OrganizationDetails getSelected() {
        return selected;
    }

    public void setSelected(OrganizationDetails selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private OrganizationDetailsFacade getFacade() {
        return ejbFacade;
    }

    public OrganizationDetails prepareCreate() {
        selected = new OrganizationDetails();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, 
                ResourceBundle.getBundle("com/clansofts/epay/configs").
                        getString("OrganizationDetailsCreated"));
        if (!ClansoftsUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("OrganizationDetailsUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("OrganizationDetailsDeleted"));
        if (!ClansoftsUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<OrganizationDetails> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                
                
                if (persistAction == PersistAction.UPDATE) {
                    getFacade().edit(selected);
                } else if (PersistAction.CREATE == persistAction) {
                    selected.setOrganizationID(this.getFacade().getNextEntityDataId());
                    getFacade().create(selected);
                }else {
                    getFacade().remove(selected);
                }
                ClansoftsUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    ClansoftsUtil.addErrorMessage(msg);
                } else {
                    ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
            }
        }
    }

    public OrganizationDetails getOrganizationDetails(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<OrganizationDetails> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<OrganizationDetails> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = OrganizationDetails.class)
    public static class OrganizationDetailsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            OrganizationDetailsController controller = (OrganizationDetailsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "organizationDetailsController");
            return controller.getOrganizationDetails(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof OrganizationDetails) {
                OrganizationDetails o = (OrganizationDetails) object;
                return getStringKey(o.getOrganizationID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), OrganizationDetails.class.getName()});
                return null;
            }
        }

    }

}
