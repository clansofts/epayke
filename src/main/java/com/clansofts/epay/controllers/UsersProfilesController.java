package com.clansofts.epay.controllers;

import com.clansofts.epay.entities.UsersProfiles;
import com.clansofts.epay.controllers.util.ClansoftsUtil;
import com.clansofts.epay.controllers.util.ClansoftsUtil.PersistAction;
import com.clansofts.epay.facades.UsersProfilesFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("usersProfilesController")
@SessionScoped
public class UsersProfilesController implements Serializable {

    @EJB
    private com.clansofts.epay.facades.UsersProfilesFacade ejbFacade;
    private List<UsersProfiles> items = null;
    private UsersProfiles selected;
    private UsersProfiles usersProfile;

    public UsersProfilesController() {
        usersProfile = new UsersProfiles();
    }

    public UsersProfiles getSelected() {
        return selected;
    }

    public void setSelected(UsersProfiles selected) {
        this.selected = selected;
    }

    /**
     * @return the usersProfile
     */
    public UsersProfiles getUsersProfile() {
        return usersProfile;
    }

    /**
     * @param usersProfile the usersProfile to set
     */
    public void setUsersProfile(UsersProfiles usersProfile) {
        this.usersProfile = usersProfile;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private UsersProfilesFacade getFacade() {
        return ejbFacade;
    }

    public UsersProfiles prepareCreate() {
        selected = new UsersProfiles();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("UsersProfilesCreated"));
        if (!ClansoftsUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("UsersProfilesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("UsersProfilesDeleted"));
        if (!ClansoftsUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<UsersProfiles> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                ClansoftsUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    ClansoftsUtil.addErrorMessage(msg);
                } else {
                    ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                ClansoftsUtil.addErrorMessage(ex, ResourceBundle.getBundle("com/clansofts/epay/configs").getString("PersistenceErrorOccured"));
            }
        }
    }

    public UsersProfiles getUsersProfiles(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<UsersProfiles> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<UsersProfiles> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = UsersProfiles.class)
    public static class UsersProfilesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsersProfilesController controller = (UsersProfilesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usersProfilesController");
            return controller.getUsersProfiles(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof UsersProfiles) {
                UsersProfiles o = (UsersProfiles) object;
                return getStringKey(o.getUserProfileID());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), UsersProfiles.class.getName()});
                return null;
            }
        }

    }

}
