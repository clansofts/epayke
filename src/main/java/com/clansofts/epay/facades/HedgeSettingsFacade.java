/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.facades;

import com.clansofts.epay.entities.HedgeSettings;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author clansofts
 */
@Stateless
public class HedgeSettingsFacade extends AbstractFacade<HedgeSettings> {
    @PersistenceContext(unitName = "EpayKE_1.0.1_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HedgeSettingsFacade() {
        super(HedgeSettings.class);
    }
    
}
