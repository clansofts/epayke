/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.facades;

import com.clansofts.epay.entities.OrganizationDetails;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author clansofts
 */
@Stateless
public class OrganizationDetailsFacade extends AbstractFacade<OrganizationDetails> {

    @PersistenceContext(unitName = "EpayKE_1.0.1_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrganizationDetailsFacade() {
        super(OrganizationDetails.class);
    }

    /**
     * Getting the next Entity ID in the Database Table
     *
     * @return
     */
    public Integer getNextEntityDataId() {
        Integer entityDataId;
        String qs = "Select max(o.organizationID) from OrganizationDetails o";
        entityDataId = (Integer) em.createQuery(qs).getSingleResult();
        if (entityDataId == null) {
            entityDataId = 0;
        }
        return (entityDataId + 1);
    }

}
