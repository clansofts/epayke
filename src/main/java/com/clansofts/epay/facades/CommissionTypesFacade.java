/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.facades;

import com.clansofts.epay.entities.CommissionTypes;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author clansofts
 */
@Stateless
public class CommissionTypesFacade extends AbstractFacade<CommissionTypes> {
    @PersistenceContext(unitName = "EpayKE_1.0.1_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommissionTypesFacade() {
        super(CommissionTypes.class);
    }
    
}
