/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.facades;

import com.clansofts.epay.entities.Customer;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author clansofts
 */
@Stateless
public class CustomerFacade extends AbstractFacade<Customer> {
    @PersistenceContext(unitName = "EpayKE_1.0.1_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomerFacade() {
        super(Customer.class);
    }
    
    /**
     * Getting the next Entity ID in the Database Table
     *
     * @return
     */
    public Integer getNextEntityDataId() {
        Integer entityDataId;
        String qs = "Select max(c.customerID) from Customer c";
        entityDataId = (Integer) em.createQuery(qs).getSingleResult();
        if (entityDataId == null) {
            entityDataId = 0;
        }
        return (entityDataId + 1);
    }
}
