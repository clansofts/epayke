/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay;

import com.sun.faces.context.FacesFileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author clansofts
 */
@WebServlet(name = "Routes",
        loadOnStartup = 1,
        urlPatterns = {
            "/index",
            "/home",
            "/login",
            "/reset_password",
            "/logout",
            "/profile",
            "/register",
            "/organizations",
            "/organizations_types",
            "/organizations_status",
            "/countries",
            "/currencies",
            "/forex_rates",
            "/hedge_settings",
            "/card",
            "/cards_types",
            "/cards_transactions",
            "/commission_structure",
            "/commission_types",
            "/employees",
            "/customers",
            "/payment_channels",
            "/recipients",
            "/recipient_wallet",
            "/modules",
            "/submodules",
            "/module_permission",
            "/submodule_permission",
            "/transfers",
            "/transfers_log",
            "/transfers_status",
            "/users_profiles",
            "/users_activities",
            "/users_types",
            "/roles_center",
            "/roles_status",})
public class Routes extends HttpServlet {

    //Creating A logger to Log Any Failure in the URL 
    private static final Logger logger = Logger.getLogger(Routes.class.getName());
    //Default Error Page on Page Resolution not found
    private final String error_404_Page = "/error_pages/error_404";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();

        switch (userPath) {
            case "/index":
                userPath = "/index";
                break;
            case "/logout":
                //Clearing the session                
                userPath = "/index";
                //Invalidate session to disallow any logins
                HttpSession session = request.getSession(false);
                if (session != null) {
                    session.invalidate();
                }
                break;

            case "/home":
                userPath = "/modules/dashboard";
                break;
            case "/profile":
                userPath = "/modules/profile";
                break;
            case "/login":
                userPath = "/index";
                break;
            case "/reset_password":
                userPath = "/reset_password";
                break;
            case "/register":
                userPath = "/register";
                break;

            //Institution Management    
            case "/organizations":
                userPath = "/modules/organizationDetails/List";
                break;
            case "/organizations_types":
                userPath = "/modules/organizationType/List";
                break;
            case "/organizations_status":
                userPath = "/modules/organizationStatus/List";
                break;
            case "/employees":
                userPath = "/modules/employees/List";
                break;

            // Settings Modules
            case "/commission_types":
                userPath = "/modules/commissionTypes/List";
                break;
            case "/cards_types":
                userPath = "/modules/cardTypes/List";
                break;
            case "/hedge_settings":
                userPath = "/modules/hedgeSettings/List";
                break;
            case "/forex_rates":
                userPath = "/modules/forexRates/List";
                break;
            case "/currencies":
                userPath = "/modules/currencies/List";
                break;
            case "/countries":
                userPath = "/modules/countries/List";
                break;
            case "/commission_structure":
                userPath = "/modules/commissionStructure/List";
                break;

            //Transactions Modules
            case "/transfers_log":
                userPath = "/modules/transfersLog/List";
                break;
            case "/transfers_status":
                userPath = "/modules/transfersStatus/List";
                break;
            case "/transfers":
                userPath = "/modules/transfer/List";
                break;
            case "/payment_channels":
                userPath = "/modules/paymentChannels/List";
                break;
            case "/cards_transactions":
                userPath = "/modules/cardTransactions/List";
                break;

            //Customers Module
            case "/card":
                userPath = "/modules/card/List";
                break;
            case "/customers":
                userPath = "/modules/customer/List";
                break;

            //Members
            case "/recipients":
                userPath = "/modules/receipients/List";
                break;
            case "/recipient_wallet":
                userPath = "/modules/receipientsWallet/List";
                break;

            //Roles & Permissions Center
            case "/roles_center":
                userPath = "/modules/rolesCenter/List";
                break;
            case "/roles_status":
                userPath = "/modules/rolesStatus/List";
                break;
            case "/users_types":
                userPath = "/modules/usersTypes/List";
                break;
            case "/users_activities":
                userPath = "/modules/usersActivities/List";
                break;
            case "/users_profiles":
                userPath = "/modules/usersProfiles/List";
                break;
            case "/modules":
                userPath = "/modules/modules/List";
                break;
            case "/submodules":
                userPath = "/modules/subModules/List";
                break;
            case "/module_permission":
                userPath = "/modules/roleModulePermission/List";
                break;
            case "/submodule_permission":
                userPath = "/modules/roleSubModulePermission/List";
                break;

        }

        String url = userPath + ".xhtml";

        try {

            request.getRequestDispatcher(url).forward(request, response);

            String loggerString = "Page Redirection Jumps to" + url;

            logger.log(Level.INFO, loggerString);

        } catch (FacesFileNotFoundException ex) {

            String urlNotFound = "/modules" + error_404_Page + ".xhtml";
            //Then Dispatch the Url
            request.getRequestDispatcher(urlNotFound).forward(request, response);

            String loggMsg = url + " Not Found in ExternalContext as a Resource ";

            //You should Redirect to Error 404 with message page not found            
            logger.log(Level.SEVERE, loggMsg);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
