/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "RolesStatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolesStatus.findAll", query = "SELECT r FROM RolesStatus r"),
    @NamedQuery(name = "RolesStatus.findByRoleStatusID", query = "SELECT r FROM RolesStatus r WHERE r.roleStatusID = :roleStatusID"),
    @NamedQuery(name = "RolesStatus.findByRolesStatusDesc", query = "SELECT r FROM RolesStatus r WHERE r.rolesStatusDesc = :rolesStatusDesc")})
public class RolesStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RoleStatusID")
    private Integer roleStatusID;
    @Size(max = 100)
    @Column(name = "RolesStatusDesc")
    private String rolesStatusDesc;
    @OneToMany(mappedBy = "roleStatus")
    private List<RolesCenter> rolesCenterList;

    public RolesStatus() {
    }

    public RolesStatus(Integer roleStatusID) {
        this.roleStatusID = roleStatusID;
    }

    public Integer getRoleStatusID() {
        return roleStatusID;
    }

    public void setRoleStatusID(Integer roleStatusID) {
        this.roleStatusID = roleStatusID;
    }

    public String getRolesStatusDesc() {
        return rolesStatusDesc;
    }

    public void setRolesStatusDesc(String rolesStatusDesc) {
        this.rolesStatusDesc = rolesStatusDesc;
    }

    @XmlTransient
    public List<RolesCenter> getRolesCenterList() {
        return rolesCenterList;
    }

    public void setRolesCenterList(List<RolesCenter> rolesCenterList) {
        this.rolesCenterList = rolesCenterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleStatusID != null ? roleStatusID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolesStatus)) {
            return false;
        }
        RolesStatus other = (RolesStatus) object;
        if ((this.roleStatusID == null && other.roleStatusID != null) || (this.roleStatusID != null && !this.roleStatusID.equals(other.roleStatusID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.RolesStatus[ roleStatusID=" + roleStatusID + " ]";
    }
    
}
