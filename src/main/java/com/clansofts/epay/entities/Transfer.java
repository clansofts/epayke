/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "Transfer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transfer.findAll", query = "SELECT t FROM Transfer t"),
    @NamedQuery(name = "Transfer.findByTransferID", query = "SELECT t FROM Transfer t WHERE t.transferID = :transferID"),
    @NamedQuery(name = "Transfer.findByTransferCode", query = "SELECT t FROM Transfer t WHERE t.transferCode = :transferCode"),
    @NamedQuery(name = "Transfer.findBySentAmount", query = "SELECT t FROM Transfer t WHERE t.sentAmount = :sentAmount"),
    @NamedQuery(name = "Transfer.findByCommissionLevied", query = "SELECT t FROM Transfer t WHERE t.commissionLevied = :commissionLevied"),
    @NamedQuery(name = "Transfer.findByReceipientAmount", query = "SELECT t FROM Transfer t WHERE t.receipientAmount = :receipientAmount"),
    @NamedQuery(name = "Transfer.findByTransferDate", query = "SELECT t FROM Transfer t WHERE t.transferDate = :transferDate")})
public class Transfer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TransferID")
    private Long transferID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TransferCode")
    private String transferCode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SentAmount")
    private Double sentAmount;
    @Column(name = "CommissionLevied")
    private Double commissionLevied;
    @Column(name = "ReceipientAmount")
    private Double receipientAmount;
    @Column(name = "TransferDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transferDate;
    @OneToMany(mappedBy = "transferInfo")
    private List<TransfersLog> transfersLogList;
    @JoinColumn(name = "CardTransactionDets", referencedColumnName = "CardDetailsID")
    @ManyToOne
    private CardTransactions cardTransactionDets;
    @JoinColumn(name = "PaymentChannel", referencedColumnName = "ChannelID")
    @ManyToOne
    private PaymentChannels paymentChannel;
    @JoinColumn(name = "ReceipientCurrency", referencedColumnName = "CurrenciesID")
    @ManyToOne
    private Currencies receipientCurrency;
    @JoinColumn(name = "SendCurrency", referencedColumnName = "CurrenciesID")
    @ManyToOne
    private Currencies sendCurrency;
    @JoinColumn(name = "Receipient", referencedColumnName = "ReceipientsID")
    @ManyToOne
    private Receipients receipient;
    @JoinColumn(name = "Initiator", referencedColumnName = "CustomerID")
    @ManyToOne
    private Customer initiator;
    @JoinColumn(name = "TransferFxRate", referencedColumnName = "ForexRatesID")
    @ManyToOne
    private ForexRates transferFxRate;
    @JoinColumn(name = "TransferStatus", referencedColumnName = "StatusID")
    @ManyToOne
    private TransfersStatus transferStatus;

    public Transfer() {
    }

    public Transfer(Long transferID) {
        this.transferID = transferID;
    }

    public Transfer(Long transferID, String transferCode) {
        this.transferID = transferID;
        this.transferCode = transferCode;
    }

    public Long getTransferID() {
        return transferID;
    }

    public void setTransferID(Long transferID) {
        this.transferID = transferID;
    }

    public String getTransferCode() {
        return transferCode;
    }

    public void setTransferCode(String transferCode) {
        this.transferCode = transferCode;
    }

    public Double getSentAmount() {
        return sentAmount;
    }

    public void setSentAmount(Double sentAmount) {
        this.sentAmount = sentAmount;
    }

    public Double getCommissionLevied() {
        return commissionLevied;
    }

    public void setCommissionLevied(Double commissionLevied) {
        this.commissionLevied = commissionLevied;
    }

    public Double getReceipientAmount() {
        return receipientAmount;
    }

    public void setReceipientAmount(Double receipientAmount) {
        this.receipientAmount = receipientAmount;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    @XmlTransient
    public List<TransfersLog> getTransfersLogList() {
        return transfersLogList;
    }

    public void setTransfersLogList(List<TransfersLog> transfersLogList) {
        this.transfersLogList = transfersLogList;
    }

    public CardTransactions getCardTransactionDets() {
        return cardTransactionDets;
    }

    public void setCardTransactionDets(CardTransactions cardTransactionDets) {
        this.cardTransactionDets = cardTransactionDets;
    }

    public PaymentChannels getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(PaymentChannels paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public Currencies getReceipientCurrency() {
        return receipientCurrency;
    }

    public void setReceipientCurrency(Currencies receipientCurrency) {
        this.receipientCurrency = receipientCurrency;
    }

    public Currencies getSendCurrency() {
        return sendCurrency;
    }

    public void setSendCurrency(Currencies sendCurrency) {
        this.sendCurrency = sendCurrency;
    }

    public Receipients getReceipient() {
        return receipient;
    }

    public void setReceipient(Receipients receipient) {
        this.receipient = receipient;
    }

    public Customer getInitiator() {
        return initiator;
    }

    public void setInitiator(Customer initiator) {
        this.initiator = initiator;
    }

    public ForexRates getTransferFxRate() {
        return transferFxRate;
    }

    public void setTransferFxRate(ForexRates transferFxRate) {
        this.transferFxRate = transferFxRate;
    }

    public TransfersStatus getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(TransfersStatus transferStatus) {
        this.transferStatus = transferStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transferID != null ? transferID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transfer)) {
            return false;
        }
        Transfer other = (Transfer) object;
        if ((this.transferID == null && other.transferID != null) || (this.transferID != null && !this.transferID.equals(other.transferID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.Transfer[ transferID=" + transferID + " ]";
    }
    
}
