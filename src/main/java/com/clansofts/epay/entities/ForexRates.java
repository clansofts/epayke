/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "ForexRates")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ForexRates.findAll", query = "SELECT f FROM ForexRates f"),
    @NamedQuery(name = "ForexRates.findByForexRatesID", query = "SELECT f FROM ForexRates f WHERE f.forexRatesID = :forexRatesID"),
    @NamedQuery(name = "ForexRates.findByExchangeRate", query = "SELECT f FROM ForexRates f WHERE f.exchangeRate = :exchangeRate"),
    @NamedQuery(name = "ForexRates.findByHedgeRate", query = "SELECT f FROM ForexRates f WHERE f.hedgeRate = :hedgeRate"),
    @NamedQuery(name = "ForexRates.findByDifference", query = "SELECT f FROM ForexRates f WHERE f.difference = :difference"),
    @NamedQuery(name = "ForexRates.findByRateAppicationDate", query = "SELECT f FROM ForexRates f WHERE f.rateAppicationDate = :rateAppicationDate"),
    @NamedQuery(name = "ForexRates.findByLoggedBy", query = "SELECT f FROM ForexRates f WHERE f.loggedBy = :loggedBy"),
    @NamedQuery(name = "ForexRates.findByLoggedOn", query = "SELECT f FROM ForexRates f WHERE f.loggedOn = :loggedOn")})
public class ForexRates implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ForexRatesID")
    private Integer forexRatesID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ExchangeRate")
    private Double exchangeRate;
    @Column(name = "HedgeRate")
    private Double hedgeRate;
    @Column(name = "Difference")
    private Double difference;
    @Column(name = "RateAppicationDate")
    @Temporal(TemporalType.DATE)
    private Date rateAppicationDate;
    @Column(name = "LoggedBy")
    private Integer loggedBy;
    @Column(name = "LoggedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loggedOn;
    @JoinColumn(name = "HedgeComputation", referencedColumnName = "HedgeSettingID")
    @ManyToOne
    private HedgeSettings hedgeComputation;
    @JoinColumn(name = "ToCurrency", referencedColumnName = "CurrenciesID")
    @ManyToOne
    private Currencies toCurrency;
    @JoinColumn(name = "FromCurrency", referencedColumnName = "CurrenciesID")
    @ManyToOne
    private Currencies fromCurrency;
    @OneToMany(mappedBy = "transferFxRate")
    private List<Transfer> transferList;

    public ForexRates() {
    }

    public ForexRates(Integer forexRatesID) {
        this.forexRatesID = forexRatesID;
    }

    public Integer getForexRatesID() {
        return forexRatesID;
    }

    public void setForexRatesID(Integer forexRatesID) {
        this.forexRatesID = forexRatesID;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Double getHedgeRate() {
        return hedgeRate;
    }

    public void setHedgeRate(Double hedgeRate) {
        this.hedgeRate = hedgeRate;
    }

    public Double getDifference() {
        return difference;
    }

    public void setDifference(Double difference) {
        this.difference = difference;
    }

    public Date getRateAppicationDate() {
        return rateAppicationDate;
    }

    public void setRateAppicationDate(Date rateAppicationDate) {
        this.rateAppicationDate = rateAppicationDate;
    }

    public Integer getLoggedBy() {
        return loggedBy;
    }

    public void setLoggedBy(Integer loggedBy) {
        this.loggedBy = loggedBy;
    }

    public Date getLoggedOn() {
        return loggedOn;
    }

    public void setLoggedOn(Date loggedOn) {
        this.loggedOn = loggedOn;
    }

    public HedgeSettings getHedgeComputation() {
        return hedgeComputation;
    }

    public void setHedgeComputation(HedgeSettings hedgeComputation) {
        this.hedgeComputation = hedgeComputation;
    }

    public Currencies getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(Currencies toCurrency) {
        this.toCurrency = toCurrency;
    }

    public Currencies getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(Currencies fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (forexRatesID != null ? forexRatesID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ForexRates)) {
            return false;
        }
        ForexRates other = (ForexRates) object;
        if ((this.forexRatesID == null && other.forexRatesID != null) || (this.forexRatesID != null && !this.forexRatesID.equals(other.forexRatesID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.ForexRates[ forexRatesID=" + forexRatesID + " ]";
    }
    
}
