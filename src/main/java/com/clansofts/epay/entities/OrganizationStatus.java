/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "OrganizationStatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrganizationStatus.findAll", query = "SELECT o FROM OrganizationStatus o"),
    @NamedQuery(name = "OrganizationStatus.findByOrganizationStatusID", query = "SELECT o FROM OrganizationStatus o WHERE o.organizationStatusID = :organizationStatusID"),
    @NamedQuery(name = "OrganizationStatus.findByOrganizationStatusDesc", query = "SELECT o FROM OrganizationStatus o WHERE o.organizationStatusDesc = :organizationStatusDesc")})
public class OrganizationStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OrganizationStatusID")
    private Integer organizationStatusID;
    @Size(max = 100)
    @Column(name = "OrganizationStatusDesc")
    private String organizationStatusDesc;
    @OneToMany(mappedBy = "organizationStatus")
    private List<OrganizationDetails> organizationDetailsList;

    public OrganizationStatus() {
    }

    public OrganizationStatus(Integer organizationStatusID) {
        this.organizationStatusID = organizationStatusID;
    }

    public Integer getOrganizationStatusID() {
        return organizationStatusID;
    }

    public void setOrganizationStatusID(Integer organizationStatusID) {
        this.organizationStatusID = organizationStatusID;
    }

    public String getOrganizationStatusDesc() {
        return organizationStatusDesc;
    }

    public void setOrganizationStatusDesc(String organizationStatusDesc) {
        this.organizationStatusDesc = organizationStatusDesc;
    }

    @XmlTransient
    public List<OrganizationDetails> getOrganizationDetailsList() {
        return organizationDetailsList;
    }

    public void setOrganizationDetailsList(List<OrganizationDetails> organizationDetailsList) {
        this.organizationDetailsList = organizationDetailsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organizationStatusID != null ? organizationStatusID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizationStatus)) {
            return false;
        }
        OrganizationStatus other = (OrganizationStatus) object;
        if ((this.organizationStatusID == null && other.organizationStatusID != null) || (this.organizationStatusID != null && !this.organizationStatusID.equals(other.organizationStatusID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.OrganizationStatus[ organizationStatusID=" + organizationStatusID + " ]";
    }
    
}
