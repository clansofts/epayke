/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "Employees")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employees.findAll", query = "SELECT e FROM Employees e"),
    @NamedQuery(name = "Employees.findByEmployeesID", query = "SELECT e FROM Employees e WHERE e.employeesID = :employeesID"),
    @NamedQuery(name = "Employees.findByEmployeeCode", query = "SELECT e FROM Employees e WHERE e.employeeCode = :employeeCode"),
    @NamedQuery(name = "Employees.findByEmployeeName", query = "SELECT e FROM Employees e WHERE e.employeeName = :employeeName"),
    @NamedQuery(name = "Employees.findByEmployeeEmailAddress", query = "SELECT e FROM Employees e WHERE e.employeeEmailAddress = :employeeEmailAddress"),
    @NamedQuery(name = "Employees.findByEmployeePhoneNo", query = "SELECT e FROM Employees e WHERE e.employeePhoneNo = :employeePhoneNo"),
    @NamedQuery(name = "Employees.findByEmployeeLogDate", query = "SELECT e FROM Employees e WHERE e.employeeLogDate = :employeeLogDate")})
public class Employees implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EmployeesID")
    private Integer employeesID;
    @Size(max = 45)
    @Column(name = "EmployeeCode")
    private String employeeCode;
    @Size(max = 250)
    @Column(name = "EmployeeName")
    private String employeeName;
    @Size(max = 250)
    @Column(name = "EmployeeEmailAddress")
    private String employeeEmailAddress;
    @Size(max = 50)
    @Column(name = "EmployeePhoneNo")
    private String employeePhoneNo;
    @Size(max = 45)
    @Column(name = "EmployeeLogDate")
    private String employeeLogDate;
    @JoinColumn(name = "EmployeeOrganization", referencedColumnName = "OrganizationID")
    @ManyToOne
    private OrganizationDetails employeeOrganization;
    @OneToMany(mappedBy = "userDetails")
    private List<UsersProfiles> usersProfilesList;

    public Employees() {
    }

    public Employees(Integer employeesID) {
        this.employeesID = employeesID;
    }

    public Integer getEmployeesID() {
        return employeesID;
    }

    public void setEmployeesID(Integer employeesID) {
        this.employeesID = employeesID;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeEmailAddress() {
        return employeeEmailAddress;
    }

    public void setEmployeeEmailAddress(String employeeEmailAddress) {
        this.employeeEmailAddress = employeeEmailAddress;
    }

    public String getEmployeePhoneNo() {
        return employeePhoneNo;
    }

    public void setEmployeePhoneNo(String employeePhoneNo) {
        this.employeePhoneNo = employeePhoneNo;
    }

    public String getEmployeeLogDate() {
        return employeeLogDate;
    }

    public void setEmployeeLogDate(String employeeLogDate) {
        this.employeeLogDate = employeeLogDate;
    }

    public OrganizationDetails getEmployeeOrganization() {
        return employeeOrganization;
    }

    public void setEmployeeOrganization(OrganizationDetails employeeOrganization) {
        this.employeeOrganization = employeeOrganization;
    }

    @XmlTransient
    public List<UsersProfiles> getUsersProfilesList() {
        return usersProfilesList;
    }

    public void setUsersProfilesList(List<UsersProfiles> usersProfilesList) {
        this.usersProfilesList = usersProfilesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeesID != null ? employeesID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employees)) {
            return false;
        }
        Employees other = (Employees) object;
        if ((this.employeesID == null && other.employeesID != null) || (this.employeesID != null && !this.employeesID.equals(other.employeesID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.Employees[ employeesID=" + employeesID + " ]";
    }
    
}
