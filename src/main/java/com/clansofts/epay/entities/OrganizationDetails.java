/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "OrganizationDetails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrganizationDetails.findAll", query = "SELECT o FROM OrganizationDetails o"),
    @NamedQuery(name = "OrganizationDetails.findByOrganizationID", query = "SELECT o FROM OrganizationDetails o WHERE o.organizationID = :organizationID"),
    @NamedQuery(name = "OrganizationDetails.findByOrganizationParent", query = "SELECT o FROM OrganizationDetails o WHERE o.organizationParent = :organizationParent"),
    @NamedQuery(name = "OrganizationDetails.findByOrganizationCode", query = "SELECT o FROM OrganizationDetails o WHERE o.organizationCode = :organizationCode"),
    @NamedQuery(name = "OrganizationDetails.findByOrganizationName", query = "SELECT o FROM OrganizationDetails o WHERE o.organizationName = :organizationName"),
    @NamedQuery(name = "OrganizationDetails.findByAddress", query = "SELECT o FROM OrganizationDetails o WHERE o.address = :address"),
    @NamedQuery(name = "OrganizationDetails.findByPhoneNumber", query = "SELECT o FROM OrganizationDetails o WHERE o.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "OrganizationDetails.findByOpeningHours", query = "SELECT o FROM OrganizationDetails o WHERE o.openingHours = :openingHours"),
    @NamedQuery(name = "OrganizationDetails.findByClosingHour", query = "SELECT o FROM OrganizationDetails o WHERE o.closingHour = :closingHour")})
public class OrganizationDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OrganizationID")
    private Integer organizationID;
    @Column(name = "OrganizationParent")
    private Integer organizationParent;
    @Size(max = 45)
    @Column(name = "OrganizationCode")
    private String organizationCode;
    @Size(max = 250)
    @Column(name = "OrganizationName")
    private String organizationName;
    @Size(max = 250)
    @Column(name = "Address")
    private String address;
    @Size(max = 45)
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @Size(max = 45)
    @Column(name = "OpeningHours")
    private String openingHours;
    @Size(max = 45)
    @Column(name = "ClosingHour")
    private String closingHour;
    @JoinColumn(name = "OrganizationStatus", referencedColumnName = "OrganizationStatusID")
    @ManyToOne
    private OrganizationStatus organizationStatus;
    @JoinColumn(name = "OrganizationTypeInf", referencedColumnName = "OrganizationTypeID")
    @ManyToOne
    private OrganizationType organizationTypeInf;
    @OneToMany(mappedBy = "employeeOrganization")
    private List<Employees> employeesList;
    @OneToMany(mappedBy = "roleOrganization")
    private List<RolesCenter> rolesCenterList;

    public OrganizationDetails() {
    }

    public OrganizationDetails(Integer organizationID) {
        this.organizationID = organizationID;
    }

    public Integer getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(Integer organizationID) {
        this.organizationID = organizationID;
    }

    public Integer getOrganizationParent() {
        return organizationParent;
    }

    public void setOrganizationParent(Integer organizationParent) {
        this.organizationParent = organizationParent;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getClosingHour() {
        return closingHour;
    }

    public void setClosingHour(String closingHour) {
        this.closingHour = closingHour;
    }

    public OrganizationStatus getOrganizationStatus() {
        return organizationStatus;
    }

    public void setOrganizationStatus(OrganizationStatus organizationStatus) {
        this.organizationStatus = organizationStatus;
    }

    public OrganizationType getOrganizationTypeInf() {
        return organizationTypeInf;
    }

    public void setOrganizationTypeInf(OrganizationType organizationTypeInf) {
        this.organizationTypeInf = organizationTypeInf;
    }

    @XmlTransient
    public List<Employees> getEmployeesList() {
        return employeesList;
    }

    public void setEmployeesList(List<Employees> employeesList) {
        this.employeesList = employeesList;
    }

    @XmlTransient
    public List<RolesCenter> getRolesCenterList() {
        return rolesCenterList;
    }

    public void setRolesCenterList(List<RolesCenter> rolesCenterList) {
        this.rolesCenterList = rolesCenterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organizationID != null ? organizationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizationDetails)) {
            return false;
        }
        OrganizationDetails other = (OrganizationDetails) object;
        if ((this.organizationID == null && other.organizationID != null) || (this.organizationID != null && !this.organizationID.equals(other.organizationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.OrganizationDetails[ organizationID=" + organizationID + " ]";
    }
    
}
