/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "OrganizationType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrganizationType.findAll", query = "SELECT o FROM OrganizationType o"),
    @NamedQuery(name = "OrganizationType.findByOrganizationTypeID", query = "SELECT o FROM OrganizationType o WHERE o.organizationTypeID = :organizationTypeID"),
    @NamedQuery(name = "OrganizationType.findByOrganizationTypeDesc", query = "SELECT o FROM OrganizationType o WHERE o.organizationTypeDesc = :organizationTypeDesc")})
public class OrganizationType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OrganizationTypeID")
    private Integer organizationTypeID;
    @Size(max = 100)
    @Column(name = "OrganizationTypeDesc")
    private String organizationTypeDesc;
    @OneToMany(mappedBy = "organizationTypeInf")
    private List<OrganizationDetails> organizationDetailsList;

    public OrganizationType() {
    }

    public OrganizationType(Integer organizationTypeID) {
        this.organizationTypeID = organizationTypeID;
    }

    public Integer getOrganizationTypeID() {
        return organizationTypeID;
    }

    public void setOrganizationTypeID(Integer organizationTypeID) {
        this.organizationTypeID = organizationTypeID;
    }

    public String getOrganizationTypeDesc() {
        return organizationTypeDesc;
    }

    public void setOrganizationTypeDesc(String organizationTypeDesc) {
        this.organizationTypeDesc = organizationTypeDesc;
    }

    @XmlTransient
    public List<OrganizationDetails> getOrganizationDetailsList() {
        return organizationDetailsList;
    }

    public void setOrganizationDetailsList(List<OrganizationDetails> organizationDetailsList) {
        this.organizationDetailsList = organizationDetailsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organizationTypeID != null ? organizationTypeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizationType)) {
            return false;
        }
        OrganizationType other = (OrganizationType) object;
        if ((this.organizationTypeID == null && other.organizationTypeID != null) || (this.organizationTypeID != null && !this.organizationTypeID.equals(other.organizationTypeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.OrganizationType[ organizationTypeID=" + organizationTypeID + " ]";
    }
    
}
