/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "RolesCenter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolesCenter.findAll", query = "SELECT r FROM RolesCenter r"),
    @NamedQuery(name = "RolesCenter.findByRoleCenterID", query = "SELECT r FROM RolesCenter r WHERE r.roleCenterID = :roleCenterID"),
    @NamedQuery(name = "RolesCenter.findByRoleName", query = "SELECT r FROM RolesCenter r WHERE r.roleName = :roleName"),
    @NamedQuery(name = "RolesCenter.findByRoleLogDate", query = "SELECT r FROM RolesCenter r WHERE r.roleLogDate = :roleLogDate")})
public class RolesCenter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RoleCenterID")
    private Integer roleCenterID;
    @Size(max = 100)
    @Column(name = "RoleName")
    private String roleName;
    @Column(name = "RoleLogDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date roleLogDate;
    @OneToMany(mappedBy = "roleSMInfo")
    private List<RoleSubModulePermission> roleSubModulePermissionList;
    @OneToMany(mappedBy = "roleInfo")
    private List<RoleModulePermission> roleModulePermissionList;
    @JoinColumn(name = "RoleStatus", referencedColumnName = "RoleStatusID")
    @ManyToOne
    private RolesStatus roleStatus;
    @JoinColumn(name = "RoleOrganization", referencedColumnName = "OrganizationID")
    @ManyToOne
    private OrganizationDetails roleOrganization;
    @OneToMany(mappedBy = "roleDets")
    private List<UsersProfiles> usersProfilesList;

    public RolesCenter() {
    }

    public RolesCenter(Integer roleCenterID) {
        this.roleCenterID = roleCenterID;
    }

    public Integer getRoleCenterID() {
        return roleCenterID;
    }

    public void setRoleCenterID(Integer roleCenterID) {
        this.roleCenterID = roleCenterID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Date getRoleLogDate() {
        return roleLogDate;
    }

    public void setRoleLogDate(Date roleLogDate) {
        this.roleLogDate = roleLogDate;
    }

    @XmlTransient
    public List<RoleSubModulePermission> getRoleSubModulePermissionList() {
        return roleSubModulePermissionList;
    }

    public void setRoleSubModulePermissionList(List<RoleSubModulePermission> roleSubModulePermissionList) {
        this.roleSubModulePermissionList = roleSubModulePermissionList;
    }

    @XmlTransient
    public List<RoleModulePermission> getRoleModulePermissionList() {
        return roleModulePermissionList;
    }

    public void setRoleModulePermissionList(List<RoleModulePermission> roleModulePermissionList) {
        this.roleModulePermissionList = roleModulePermissionList;
    }

    public RolesStatus getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(RolesStatus roleStatus) {
        this.roleStatus = roleStatus;
    }

    public OrganizationDetails getRoleOrganization() {
        return roleOrganization;
    }

    public void setRoleOrganization(OrganizationDetails roleOrganization) {
        this.roleOrganization = roleOrganization;
    }

    @XmlTransient
    public List<UsersProfiles> getUsersProfilesList() {
        return usersProfilesList;
    }

    public void setUsersProfilesList(List<UsersProfiles> usersProfilesList) {
        this.usersProfilesList = usersProfilesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleCenterID != null ? roleCenterID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolesCenter)) {
            return false;
        }
        RolesCenter other = (RolesCenter) object;
        if ((this.roleCenterID == null && other.roleCenterID != null) || (this.roleCenterID != null && !this.roleCenterID.equals(other.roleCenterID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.RolesCenter[ roleCenterID=" + roleCenterID + " ]";
    }
    
}
