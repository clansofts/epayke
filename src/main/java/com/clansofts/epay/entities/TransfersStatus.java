/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "TransfersStatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransfersStatus.findAll", query = "SELECT t FROM TransfersStatus t"),
    @NamedQuery(name = "TransfersStatus.findByStatusID", query = "SELECT t FROM TransfersStatus t WHERE t.statusID = :statusID"),
    @NamedQuery(name = "TransfersStatus.findByStatusName", query = "SELECT t FROM TransfersStatus t WHERE t.statusName = :statusName")})
public class TransfersStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "StatusID")
    private Integer statusID;
    @Size(max = 45)
    @Column(name = "StatusName")
    private String statusName;
    @OneToMany(mappedBy = "transferStatus")
    private List<Transfer> transferList;

    public TransfersStatus() {
    }

    public TransfersStatus(Integer statusID) {
        this.statusID = statusID;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (statusID != null ? statusID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransfersStatus)) {
            return false;
        }
        TransfersStatus other = (TransfersStatus) object;
        if ((this.statusID == null && other.statusID != null) || (this.statusID != null && !this.statusID.equals(other.statusID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.TransfersStatus[ statusID=" + statusID + " ]";
    }
    
}
