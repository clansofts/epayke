/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "CommissionStructure")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CommissionStructure.findAll", query = "SELECT c FROM CommissionStructure c"),
    @NamedQuery(name = "CommissionStructure.findByStructureID", query = "SELECT c FROM CommissionStructure c WHERE c.structureID = :structureID"),
    @NamedQuery(name = "CommissionStructure.findBySendAmountFrom", query = "SELECT c FROM CommissionStructure c WHERE c.sendAmountFrom = :sendAmountFrom"),
    @NamedQuery(name = "CommissionStructure.findBySendAmountTo", query = "SELECT c FROM CommissionStructure c WHERE c.sendAmountTo = :sendAmountTo"),
    @NamedQuery(name = "CommissionStructure.findByCommission", query = "SELECT c FROM CommissionStructure c WHERE c.commission = :commission")})
public class CommissionStructure implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "StructureID")
    private Integer structureID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SendAmountFrom")
    private Double sendAmountFrom;
    @Column(name = "SendAmountTo")
    private Double sendAmountTo;
    @Column(name = "Commission")
    private Double commission;
    @JoinColumn(name = "CommissionType", referencedColumnName = "TypeID")
    @ManyToOne
    private CommissionTypes commissionType;
    @JoinColumn(name = "SendBaseCurrency", referencedColumnName = "CurrenciesID")
    @ManyToOne
    private Currencies sendBaseCurrency;

    public CommissionStructure() {
    }

    public CommissionStructure(Integer structureID) {
        this.structureID = structureID;
    }

    public Integer getStructureID() {
        return structureID;
    }

    public void setStructureID(Integer structureID) {
        this.structureID = structureID;
    }

    public Double getSendAmountFrom() {
        return sendAmountFrom;
    }

    public void setSendAmountFrom(Double sendAmountFrom) {
        this.sendAmountFrom = sendAmountFrom;
    }

    public Double getSendAmountTo() {
        return sendAmountTo;
    }

    public void setSendAmountTo(Double sendAmountTo) {
        this.sendAmountTo = sendAmountTo;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public CommissionTypes getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(CommissionTypes commissionType) {
        this.commissionType = commissionType;
    }

    public Currencies getSendBaseCurrency() {
        return sendBaseCurrency;
    }

    public void setSendBaseCurrency(Currencies sendBaseCurrency) {
        this.sendBaseCurrency = sendBaseCurrency;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (structureID != null ? structureID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommissionStructure)) {
            return false;
        }
        CommissionStructure other = (CommissionStructure) object;
        if ((this.structureID == null && other.structureID != null) || (this.structureID != null && !this.structureID.equals(other.structureID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.CommissionStructure[ structureID=" + structureID + " ]";
    }
    
}
