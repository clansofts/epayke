/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "UsersActivities")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsersActivities.findAll", query = "SELECT u FROM UsersActivities u"),
    @NamedQuery(name = "UsersActivities.findByUsersActivitiesID", query = "SELECT u FROM UsersActivities u WHERE u.usersActivitiesID = :usersActivitiesID"),
    @NamedQuery(name = "UsersActivities.findByActivityLogDate", query = "SELECT u FROM UsersActivities u WHERE u.activityLogDate = :activityLogDate")})
public class UsersActivities implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "UsersActivitiesID")
    private Long usersActivitiesID;
    @Lob
    @Size(max = 65535)
    @Column(name = "UsersActivity")
    private String usersActivity;
    @Column(name = "ActivityLogDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activityLogDate;
    @JoinColumn(name = "UserInformation", referencedColumnName = "UserType")
    @ManyToOne
    private UsersProfiles userInformation;

    public UsersActivities() {
    }

    public UsersActivities(Long usersActivitiesID) {
        this.usersActivitiesID = usersActivitiesID;
    }

    public Long getUsersActivitiesID() {
        return usersActivitiesID;
    }

    public void setUsersActivitiesID(Long usersActivitiesID) {
        this.usersActivitiesID = usersActivitiesID;
    }

    public String getUsersActivity() {
        return usersActivity;
    }

    public void setUsersActivity(String usersActivity) {
        this.usersActivity = usersActivity;
    }

    public Date getActivityLogDate() {
        return activityLogDate;
    }

    public void setActivityLogDate(Date activityLogDate) {
        this.activityLogDate = activityLogDate;
    }

    public UsersProfiles getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UsersProfiles userInformation) {
        this.userInformation = userInformation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usersActivitiesID != null ? usersActivitiesID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersActivities)) {
            return false;
        }
        UsersActivities other = (UsersActivities) object;
        if ((this.usersActivitiesID == null && other.usersActivitiesID != null) || (this.usersActivitiesID != null && !this.usersActivitiesID.equals(other.usersActivitiesID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.UsersActivities[ usersActivitiesID=" + usersActivitiesID + " ]";
    }
    
}
