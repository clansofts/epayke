/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "CardTypes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CardTypes.findAll", query = "SELECT c FROM CardTypes c"),
    @NamedQuery(name = "CardTypes.findByCardTypesID", query = "SELECT c FROM CardTypes c WHERE c.cardTypesID = :cardTypesID"),
    @NamedQuery(name = "CardTypes.findByCardTypeName", query = "SELECT c FROM CardTypes c WHERE c.cardTypeName = :cardTypeName")})
public class CardTypes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CardTypesID")
    private Integer cardTypesID;
    @Size(max = 45)
    @Column(name = "CardTypeName")
    private String cardTypeName;
    @OneToMany(mappedBy = "cardType")
    private List<Card> cardList;

    public CardTypes() {
    }

    public CardTypes(Integer cardTypesID) {
        this.cardTypesID = cardTypesID;
    }

    public Integer getCardTypesID() {
        return cardTypesID;
    }

    public void setCardTypesID(Integer cardTypesID) {
        this.cardTypesID = cardTypesID;
    }

    public String getCardTypeName() {
        return cardTypeName;
    }

    public void setCardTypeName(String cardTypeName) {
        this.cardTypeName = cardTypeName;
    }

    @XmlTransient
    public List<Card> getCardList() {
        return cardList;
    }

    public void setCardList(List<Card> cardList) {
        this.cardList = cardList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardTypesID != null ? cardTypesID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CardTypes)) {
            return false;
        }
        CardTypes other = (CardTypes) object;
        if ((this.cardTypesID == null && other.cardTypesID != null) || (this.cardTypesID != null && !this.cardTypesID.equals(other.cardTypesID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.CardTypes[ cardTypesID=" + cardTypesID + " ]";
    }
    
}
