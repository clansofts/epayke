/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "CardTransactions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CardTransactions.findAll", query = "SELECT c FROM CardTransactions c"),
    @NamedQuery(name = "CardTransactions.findByCardDetailsID", query = "SELECT c FROM CardTransactions c WHERE c.cardDetailsID = :cardDetailsID"),
    @NamedQuery(name = "CardTransactions.findByDeductedAmount", query = "SELECT c FROM CardTransactions c WHERE c.deductedAmount = :deductedAmount"),
    @NamedQuery(name = "CardTransactions.findByTransactionDate", query = "SELECT c FROM CardTransactions c WHERE c.transactionDate = :transactionDate"),
    @NamedQuery(name = "CardTransactions.findByTransactionStamp", query = "SELECT c FROM CardTransactions c WHERE c.transactionStamp = :transactionStamp")})
public class CardTransactions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CardDetailsID")
    private Integer cardDetailsID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DeductedAmount")
    private Double deductedAmount;
    @Column(name = "TransactionDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;
    @Column(name = "TransactionStamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionStamp;
    @JoinColumn(name = "PaymentCard", referencedColumnName = "CardID")
    @ManyToOne
    private Card paymentCard;
    @OneToMany(mappedBy = "cardTransactionDets")
    private List<Transfer> transferList;

    public CardTransactions() {
    }

    public CardTransactions(Integer cardDetailsID) {
        this.cardDetailsID = cardDetailsID;
    }

    public Integer getCardDetailsID() {
        return cardDetailsID;
    }

    public void setCardDetailsID(Integer cardDetailsID) {
        this.cardDetailsID = cardDetailsID;
    }

    public Double getDeductedAmount() {
        return deductedAmount;
    }

    public void setDeductedAmount(Double deductedAmount) {
        this.deductedAmount = deductedAmount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getTransactionStamp() {
        return transactionStamp;
    }

    public void setTransactionStamp(Date transactionStamp) {
        this.transactionStamp = transactionStamp;
    }

    public Card getPaymentCard() {
        return paymentCard;
    }

    public void setPaymentCard(Card paymentCard) {
        this.paymentCard = paymentCard;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardDetailsID != null ? cardDetailsID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CardTransactions)) {
            return false;
        }
        CardTransactions other = (CardTransactions) object;
        if ((this.cardDetailsID == null && other.cardDetailsID != null) || (this.cardDetailsID != null && !this.cardDetailsID.equals(other.cardDetailsID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.CardTransactions[ cardDetailsID=" + cardDetailsID + " ]";
    }
    
}
