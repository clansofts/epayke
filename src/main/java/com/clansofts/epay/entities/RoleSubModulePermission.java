/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "RoleSubModulePermission")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RoleSubModulePermission.findAll", query = "SELECT r FROM RoleSubModulePermission r"),
    @NamedQuery(name = "RoleSubModulePermission.findBySubModulePermissionID", query = "SELECT r FROM RoleSubModulePermission r WHERE r.subModulePermissionID = :subModulePermissionID")})
public class RoleSubModulePermission implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SubModulePermissionID")
    private Integer subModulePermissionID;
    @JoinColumn(name = "SubModuleInfo", referencedColumnName = "SubModuleID")
    @ManyToOne
    private SubModules subModuleInfo;
    @JoinColumn(name = "ModuleSInfo", referencedColumnName = "ModuleID")
    @ManyToOne
    private Modules moduleSInfo;
    @JoinColumn(name = "RoleSMInfo", referencedColumnName = "RoleCenterID")
    @ManyToOne
    private RolesCenter roleSMInfo;

    public RoleSubModulePermission() {
    }

    public RoleSubModulePermission(Integer subModulePermissionID) {
        this.subModulePermissionID = subModulePermissionID;
    }

    public Integer getSubModulePermissionID() {
        return subModulePermissionID;
    }

    public void setSubModulePermissionID(Integer subModulePermissionID) {
        this.subModulePermissionID = subModulePermissionID;
    }

    public SubModules getSubModuleInfo() {
        return subModuleInfo;
    }

    public void setSubModuleInfo(SubModules subModuleInfo) {
        this.subModuleInfo = subModuleInfo;
    }

    public Modules getModuleSInfo() {
        return moduleSInfo;
    }

    public void setModuleSInfo(Modules moduleSInfo) {
        this.moduleSInfo = moduleSInfo;
    }

    public RolesCenter getRoleSMInfo() {
        return roleSMInfo;
    }

    public void setRoleSMInfo(RolesCenter roleSMInfo) {
        this.roleSMInfo = roleSMInfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subModulePermissionID != null ? subModulePermissionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleSubModulePermission)) {
            return false;
        }
        RoleSubModulePermission other = (RoleSubModulePermission) object;
        if ((this.subModulePermissionID == null && other.subModulePermissionID != null) || (this.subModulePermissionID != null && !this.subModulePermissionID.equals(other.subModulePermissionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.RoleSubModulePermission[ subModulePermissionID=" + subModulePermissionID + " ]";
    }
    
}
