/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "Modules")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modules.findAll", query = "SELECT m FROM Modules m"),
    @NamedQuery(name = "Modules.findByModuleID", query = "SELECT m FROM Modules m WHERE m.moduleID = :moduleID"),
    @NamedQuery(name = "Modules.findByModuleName", query = "SELECT m FROM Modules m WHERE m.moduleName = :moduleName"),
    @NamedQuery(name = "Modules.findByModuleLink", query = "SELECT m FROM Modules m WHERE m.moduleLink = :moduleLink"),
    @NamedQuery(name = "Modules.findByModuleIcon", query = "SELECT m FROM Modules m WHERE m.moduleIcon = :moduleIcon"),
    @NamedQuery(name = "Modules.findByModuleLogDate", query = "SELECT m FROM Modules m WHERE m.moduleLogDate = :moduleLogDate")})
public class Modules implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ModuleID")
    private Integer moduleID;
    @Size(max = 250)
    @Column(name = "ModuleName")
    private String moduleName;
    @Size(max = 100)
    @Column(name = "ModuleLink")
    private String moduleLink;
    @Size(max = 45)
    @Column(name = "ModuleIcon")
    private String moduleIcon;
    @Column(name = "ModuleLogDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date moduleLogDate;
    @OneToMany(mappedBy = "moduleDetails")
    private List<SubModules> subModulesList;
    @OneToMany(mappedBy = "moduleSInfo")
    private List<RoleSubModulePermission> roleSubModulePermissionList;
    @OneToMany(mappedBy = "moduleInformation")
    private List<RoleModulePermission> roleModulePermissionList;

    public Modules() {
    }

    public Modules(Integer moduleID) {
        this.moduleID = moduleID;
    }

    public Integer getModuleID() {
        return moduleID;
    }

    public void setModuleID(Integer moduleID) {
        this.moduleID = moduleID;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleLink() {
        return moduleLink;
    }

    public void setModuleLink(String moduleLink) {
        this.moduleLink = moduleLink;
    }

    public String getModuleIcon() {
        return moduleIcon;
    }

    public void setModuleIcon(String moduleIcon) {
        this.moduleIcon = moduleIcon;
    }

    public Date getModuleLogDate() {
        return moduleLogDate;
    }

    public void setModuleLogDate(Date moduleLogDate) {
        this.moduleLogDate = moduleLogDate;
    }

    @XmlTransient
    public List<SubModules> getSubModulesList() {
        return subModulesList;
    }

    public void setSubModulesList(List<SubModules> subModulesList) {
        this.subModulesList = subModulesList;
    }

    @XmlTransient
    public List<RoleSubModulePermission> getRoleSubModulePermissionList() {
        return roleSubModulePermissionList;
    }

    public void setRoleSubModulePermissionList(List<RoleSubModulePermission> roleSubModulePermissionList) {
        this.roleSubModulePermissionList = roleSubModulePermissionList;
    }

    @XmlTransient
    public List<RoleModulePermission> getRoleModulePermissionList() {
        return roleModulePermissionList;
    }

    public void setRoleModulePermissionList(List<RoleModulePermission> roleModulePermissionList) {
        this.roleModulePermissionList = roleModulePermissionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (moduleID != null ? moduleID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modules)) {
            return false;
        }
        Modules other = (Modules) object;
        if ((this.moduleID == null && other.moduleID != null) || (this.moduleID != null && !this.moduleID.equals(other.moduleID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.Modules[ moduleID=" + moduleID + " ]";
    }
    
}
