/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "Receipients")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Receipients.findAll", query = "SELECT r FROM Receipients r"),
    @NamedQuery(name = "Receipients.findByReceipientsID", query = "SELECT r FROM Receipients r WHERE r.receipientsID = :receipientsID"),
    @NamedQuery(name = "Receipients.findByFirstName", query = "SELECT r FROM Receipients r WHERE r.firstName = :firstName"),
    @NamedQuery(name = "Receipients.findByLastName", query = "SELECT r FROM Receipients r WHERE r.lastName = :lastName"),
    @NamedQuery(name = "Receipients.findByEmail", query = "SELECT r FROM Receipients r WHERE r.email = :email"),
    @NamedQuery(name = "Receipients.findByCity", query = "SELECT r FROM Receipients r WHERE r.city = :city"),
    @NamedQuery(name = "Receipients.findByZipCode", query = "SELECT r FROM Receipients r WHERE r.zipCode = :zipCode"),
    @NamedQuery(name = "Receipients.findByMobileCarrier", query = "SELECT r FROM Receipients r WHERE r.mobileCarrier = :mobileCarrier"),
    @NamedQuery(name = "Receipients.findByMobileNumber", query = "SELECT r FROM Receipients r WHERE r.mobileNumber = :mobileNumber")})
public class Receipients implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ReceipientsID")
    private Integer receipientsID;
    @Size(max = 250)
    @Column(name = "FirstName")
    private String firstName;
    @Size(max = 250)
    @Column(name = "LastName")
    private String lastName;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "Email")
    private String email;
    @Size(max = 100)
    @Column(name = "City")
    private String city;
    @Size(max = 45)
    @Column(name = "ZipCode")
    private String zipCode;
    @Size(max = 100)
    @Column(name = "MobileCarrier")
    private String mobileCarrier;
    @Size(max = 50)
    @Column(name = "MobileNumber")
    private String mobileNumber;
    @JoinColumn(name = "WalletInfo", referencedColumnName = "WalletID")
    @ManyToOne
    private ReceipientsWallet walletInfo;
    @JoinColumn(name = "ReceipientOwner", referencedColumnName = "CustomerID")
    @ManyToOne
    private Customer receipientOwner;
    @JoinColumn(name = "Country", referencedColumnName = "CountryID")
    @ManyToOne
    private Countries country;
    @OneToMany(mappedBy = "receipient")
    private List<Transfer> transferList;

    public Receipients() {
    }

    public Receipients(Integer receipientsID) {
        this.receipientsID = receipientsID;
    }

    public Integer getReceipientsID() {
        return receipientsID;
    }

    public void setReceipientsID(Integer receipientsID) {
        this.receipientsID = receipientsID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getMobileCarrier() {
        return mobileCarrier;
    }

    public void setMobileCarrier(String mobileCarrier) {
        this.mobileCarrier = mobileCarrier;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ReceipientsWallet getWalletInfo() {
        return walletInfo;
    }

    public void setWalletInfo(ReceipientsWallet walletInfo) {
        this.walletInfo = walletInfo;
    }

    public Customer getReceipientOwner() {
        return receipientOwner;
    }

    public void setReceipientOwner(Customer receipientOwner) {
        this.receipientOwner = receipientOwner;
    }

    public Countries getCountry() {
        return country;
    }

    public void setCountry(Countries country) {
        this.country = country;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (receipientsID != null ? receipientsID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Receipients)) {
            return false;
        }
        Receipients other = (Receipients) object;
        if ((this.receipientsID == null && other.receipientsID != null) || (this.receipientsID != null && !this.receipientsID.equals(other.receipientsID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.Receipients[ receipientsID=" + receipientsID + " ]";
    }
    
}
