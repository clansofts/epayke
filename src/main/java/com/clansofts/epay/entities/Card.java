/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "Card")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Card.findAll", query = "SELECT c FROM Card c"),
    @NamedQuery(name = "Card.findByCardID", query = "SELECT c FROM Card c WHERE c.cardID = :cardID"),
    @NamedQuery(name = "Card.findByAccountName", query = "SELECT c FROM Card c WHERE c.accountName = :accountName"),
    @NamedQuery(name = "Card.findByCardNumber", query = "SELECT c FROM Card c WHERE c.cardNumber = :cardNumber"),
    @NamedQuery(name = "Card.findByExpiryDate", query = "SELECT c FROM Card c WHERE c.expiryDate = :expiryDate"),
    @NamedQuery(name = "Card.findByCcv", query = "SELECT c FROM Card c WHERE c.ccv = :ccv")})
public class Card implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CardID")
    private Integer cardID;
    @Size(max = 250)
    @Column(name = "AccountName")
    private String accountName;
    @Size(max = 100)
    @Column(name = "CardNumber")
    private String cardNumber;
    @Column(name = "ExpiryDate")
    @Temporal(TemporalType.DATE)
    private Date expiryDate;
    @Size(max = 3)
    @Column(name = "CCV")
    private String ccv;
    @OneToMany(mappedBy = "paymentCard")
    private List<CardTransactions> cardTransactionsList;
    @JoinColumn(name = "CardType", referencedColumnName = "CardTypesID")
    @ManyToOne
    private CardTypes cardType;
    @JoinColumn(name = "OwnerInfo", referencedColumnName = "CustomerID")
    @ManyToOne
    private Customer ownerInfo;

    public Card() {
    }

    public Card(Integer cardID) {
        this.cardID = cardID;
    }

    public Integer getCardID() {
        return cardID;
    }

    public void setCardID(Integer cardID) {
        this.cardID = cardID;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCcv() {
        return ccv;
    }

    public void setCcv(String ccv) {
        this.ccv = ccv;
    }

    @XmlTransient
    public List<CardTransactions> getCardTransactionsList() {
        return cardTransactionsList;
    }

    public void setCardTransactionsList(List<CardTransactions> cardTransactionsList) {
        this.cardTransactionsList = cardTransactionsList;
    }

    public CardTypes getCardType() {
        return cardType;
    }

    public void setCardType(CardTypes cardType) {
        this.cardType = cardType;
    }

    public Customer getOwnerInfo() {
        return ownerInfo;
    }

    public void setOwnerInfo(Customer ownerInfo) {
        this.ownerInfo = ownerInfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardID != null ? cardID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Card)) {
            return false;
        }
        Card other = (Card) object;
        if ((this.cardID == null && other.cardID != null) || (this.cardID != null && !this.cardID.equals(other.cardID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.Card[ cardID=" + cardID + " ]";
    }
    
}
