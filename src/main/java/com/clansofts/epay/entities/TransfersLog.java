/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "TransfersLog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransfersLog.findAll", query = "SELECT t FROM TransfersLog t"),
    @NamedQuery(name = "TransfersLog.findByTransfersLogID", query = "SELECT t FROM TransfersLog t WHERE t.transfersLogID = :transfersLogID"),
    @NamedQuery(name = "TransfersLog.findByLogDate", query = "SELECT t FROM TransfersLog t WHERE t.logDate = :logDate")})
public class TransfersLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TransfersLogID")
    private Long transfersLogID;
    @Column(name = "LogDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logDate;
    @Lob
    @Size(max = 65535)
    @Column(name = "LogActivity")
    private String logActivity;
    @JoinColumn(name = "TransferInfo", referencedColumnName = "TransferID")
    @ManyToOne
    private Transfer transferInfo;

    public TransfersLog() {
    }

    public TransfersLog(Long transfersLogID) {
        this.transfersLogID = transfersLogID;
    }

    public Long getTransfersLogID() {
        return transfersLogID;
    }

    public void setTransfersLogID(Long transfersLogID) {
        this.transfersLogID = transfersLogID;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public String getLogActivity() {
        return logActivity;
    }

    public void setLogActivity(String logActivity) {
        this.logActivity = logActivity;
    }

    public Transfer getTransferInfo() {
        return transferInfo;
    }

    public void setTransferInfo(Transfer transferInfo) {
        this.transferInfo = transferInfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transfersLogID != null ? transfersLogID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransfersLog)) {
            return false;
        }
        TransfersLog other = (TransfersLog) object;
        if ((this.transfersLogID == null && other.transfersLogID != null) || (this.transfersLogID != null && !this.transfersLogID.equals(other.transfersLogID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.TransfersLog[ transfersLogID=" + transfersLogID + " ]";
    }
    
}
