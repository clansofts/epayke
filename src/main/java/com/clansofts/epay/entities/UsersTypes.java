/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "UsersTypes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsersTypes.findAll", query = "SELECT u FROM UsersTypes u"),
    @NamedQuery(name = "UsersTypes.findByUserTypeID", query = "SELECT u FROM UsersTypes u WHERE u.userTypeID = :userTypeID"),
    @NamedQuery(name = "UsersTypes.findByUsersTypeDesc", query = "SELECT u FROM UsersTypes u WHERE u.usersTypeDesc = :usersTypeDesc")})
public class UsersTypes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "UserTypeID")
    private Integer userTypeID;
    @Size(max = 45)
    @Column(name = "UsersTypeDesc")
    private String usersTypeDesc;
    @OneToMany(mappedBy = "userType")
    private List<UsersProfiles> usersProfilesList;

    public UsersTypes() {
    }

    public UsersTypes(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

    public Integer getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

    public String getUsersTypeDesc() {
        return usersTypeDesc;
    }

    public void setUsersTypeDesc(String usersTypeDesc) {
        this.usersTypeDesc = usersTypeDesc;
    }

    @XmlTransient
    public List<UsersProfiles> getUsersProfilesList() {
        return usersProfilesList;
    }

    public void setUsersProfilesList(List<UsersProfiles> usersProfilesList) {
        this.usersProfilesList = usersProfilesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userTypeID != null ? userTypeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersTypes)) {
            return false;
        }
        UsersTypes other = (UsersTypes) object;
        if ((this.userTypeID == null && other.userTypeID != null) || (this.userTypeID != null && !this.userTypeID.equals(other.userTypeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.UsersTypes[ userTypeID=" + userTypeID + " ]";
    }
    
}
