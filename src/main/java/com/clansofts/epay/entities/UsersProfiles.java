/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "UsersProfiles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsersProfiles.findAll", query = "SELECT u FROM UsersProfiles u"),
    @NamedQuery(name = "UsersProfiles.findByUserProfileID", query = "SELECT u FROM UsersProfiles u WHERE u.userProfileID = :userProfileID"),
    @NamedQuery(name = "UsersProfiles.findByUserName", query = "SELECT u FROM UsersProfiles u WHERE u.userName = :userName"),
    @NamedQuery(name = "UsersProfiles.findByPassCode", query = "SELECT u FROM UsersProfiles u WHERE u.passCode = :passCode")})
public class UsersProfiles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "UserProfileID")
    private Long userProfileID;
    @Size(max = 250)
    @Column(name = "UserName")
    private String userName;
    @Size(max = 250)
    @Column(name = "PassCode")
    private String passCode;
    @JoinColumn(name = "RoleDets", referencedColumnName = "RoleCenterID")
    @ManyToOne
    private RolesCenter roleDets;
    @JoinColumn(name = "UserDetails", referencedColumnName = "EmployeesID")
    @ManyToOne
    private Employees userDetails;
    @JoinColumn(name = "CustomerDets", referencedColumnName = "CustomerID")
    @ManyToOne
    private Customer customerDets;
    @JoinColumn(name = "UserType", referencedColumnName = "UserTypeID")
    @ManyToOne
    private UsersTypes userType;
    @OneToMany(mappedBy = "userInformation")
    private List<UsersActivities> usersActivitiesList;

    public UsersProfiles() {
    }

    public UsersProfiles(Long userProfileID) {
        this.userProfileID = userProfileID;
    }

    public Long getUserProfileID() {
        return userProfileID;
    }

    public void setUserProfileID(Long userProfileID) {
        this.userProfileID = userProfileID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    public RolesCenter getRoleDets() {
        return roleDets;
    }

    public void setRoleDets(RolesCenter roleDets) {
        this.roleDets = roleDets;
    }

    public Employees getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(Employees userDetails) {
        this.userDetails = userDetails;
    }

    public Customer getCustomerDets() {
        return customerDets;
    }

    public void setCustomerDets(Customer customerDets) {
        this.customerDets = customerDets;
    }

    public UsersTypes getUserType() {
        return userType;
    }

    public void setUserType(UsersTypes userType) {
        this.userType = userType;
    }

    @XmlTransient
    public List<UsersActivities> getUsersActivitiesList() {
        return usersActivitiesList;
    }

    public void setUsersActivitiesList(List<UsersActivities> usersActivitiesList) {
        this.usersActivitiesList = usersActivitiesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userProfileID != null ? userProfileID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersProfiles)) {
            return false;
        }
        UsersProfiles other = (UsersProfiles) object;
        if ((this.userProfileID == null && other.userProfileID != null) || (this.userProfileID != null && !this.userProfileID.equals(other.userProfileID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.UsersProfiles[ userProfileID=" + userProfileID + " ]";
    }
    
}
