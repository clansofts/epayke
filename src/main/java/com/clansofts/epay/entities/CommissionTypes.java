/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "CommissionTypes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CommissionTypes.findAll", query = "SELECT c FROM CommissionTypes c"),
    @NamedQuery(name = "CommissionTypes.findByTypeID", query = "SELECT c FROM CommissionTypes c WHERE c.typeID = :typeID"),
    @NamedQuery(name = "CommissionTypes.findByTypeDesc", query = "SELECT c FROM CommissionTypes c WHERE c.typeDesc = :typeDesc")})
public class CommissionTypes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TypeID")
    private Integer typeID;
    @Size(max = 45)
    @Column(name = "TypeDesc")
    private String typeDesc;
    @OneToMany(mappedBy = "commissionType")
    private List<CommissionStructure> commissionStructureList;

    public CommissionTypes() {
    }

    public CommissionTypes(Integer typeID) {
        this.typeID = typeID;
    }

    public Integer getTypeID() {
        return typeID;
    }

    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    @XmlTransient
    public List<CommissionStructure> getCommissionStructureList() {
        return commissionStructureList;
    }

    public void setCommissionStructureList(List<CommissionStructure> commissionStructureList) {
        this.commissionStructureList = commissionStructureList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typeID != null ? typeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommissionTypes)) {
            return false;
        }
        CommissionTypes other = (CommissionTypes) object;
        if ((this.typeID == null && other.typeID != null) || (this.typeID != null && !this.typeID.equals(other.typeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.CommissionTypes[ typeID=" + typeID + " ]";
    }
    
}
