/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "ReceipientsWallet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReceipientsWallet.findAll", query = "SELECT r FROM ReceipientsWallet r"),
    @NamedQuery(name = "ReceipientsWallet.findByWalletID", query = "SELECT r FROM ReceipientsWallet r WHERE r.walletID = :walletID"),
    @NamedQuery(name = "ReceipientsWallet.findByMobileWallet", query = "SELECT r FROM ReceipientsWallet r WHERE r.mobileWallet = :mobileWallet"),
    @NamedQuery(name = "ReceipientsWallet.findByNetworkCarrier", query = "SELECT r FROM ReceipientsWallet r WHERE r.networkCarrier = :networkCarrier")})
public class ReceipientsWallet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "WalletID")
    private Integer walletID;
    @Size(max = 100)
    @Column(name = "MobileWallet")
    private String mobileWallet;
    @Size(max = 45)
    @Column(name = "NetworkCarrier")
    private String networkCarrier;
    @OneToMany(mappedBy = "walletInfo")
    private List<Receipients> receipientsList;

    public ReceipientsWallet() {
    }

    public ReceipientsWallet(Integer walletID) {
        this.walletID = walletID;
    }

    public Integer getWalletID() {
        return walletID;
    }

    public void setWalletID(Integer walletID) {
        this.walletID = walletID;
    }

    public String getMobileWallet() {
        return mobileWallet;
    }

    public void setMobileWallet(String mobileWallet) {
        this.mobileWallet = mobileWallet;
    }

    public String getNetworkCarrier() {
        return networkCarrier;
    }

    public void setNetworkCarrier(String networkCarrier) {
        this.networkCarrier = networkCarrier;
    }

    @XmlTransient
    public List<Receipients> getReceipientsList() {
        return receipientsList;
    }

    public void setReceipientsList(List<Receipients> receipientsList) {
        this.receipientsList = receipientsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (walletID != null ? walletID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReceipientsWallet)) {
            return false;
        }
        ReceipientsWallet other = (ReceipientsWallet) object;
        if ((this.walletID == null && other.walletID != null) || (this.walletID != null && !this.walletID.equals(other.walletID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.ReceipientsWallet[ walletID=" + walletID + " ]";
    }
    
}
