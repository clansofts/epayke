/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "Currencies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Currencies.findAll", query = "SELECT c FROM Currencies c"),
    @NamedQuery(name = "Currencies.findByCurrenciesID", query = "SELECT c FROM Currencies c WHERE c.currenciesID = :currenciesID"),
    @NamedQuery(name = "Currencies.findByISOCurrencyCode", query = "SELECT c FROM Currencies c WHERE c.iSOCurrencyCode = :iSOCurrencyCode"),
    @NamedQuery(name = "Currencies.findByCurrencyName", query = "SELECT c FROM Currencies c WHERE c.currencyName = :currencyName")})
public class Currencies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CurrenciesID")
    private Integer currenciesID;
    @Size(max = 10)
    @Column(name = "ISOCurrencyCode")
    private String iSOCurrencyCode;
    @Size(max = 100)
    @Column(name = "CurrencyName")
    private String currencyName;
    @OneToMany(mappedBy = "toCurrency")
    private List<ForexRates> forexRatesList;
    @OneToMany(mappedBy = "fromCurrency")
    private List<ForexRates> forexRatesList1;
    @JoinColumn(name = "CurrencyBaseCountry", referencedColumnName = "CountryID")
    @ManyToOne
    private Countries currencyBaseCountry;
    @OneToMany(mappedBy = "sendBaseCurrency")
    private List<CommissionStructure> commissionStructureList;
    @OneToMany(mappedBy = "receipientCurrency")
    private List<Transfer> transferList;
    @OneToMany(mappedBy = "sendCurrency")
    private List<Transfer> transferList1;

    public Currencies() {
    }

    public Currencies(Integer currenciesID) {
        this.currenciesID = currenciesID;
    }

    public Integer getCurrenciesID() {
        return currenciesID;
    }

    public void setCurrenciesID(Integer currenciesID) {
        this.currenciesID = currenciesID;
    }

    public String getISOCurrencyCode() {
        return iSOCurrencyCode;
    }

    public void setISOCurrencyCode(String iSOCurrencyCode) {
        this.iSOCurrencyCode = iSOCurrencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    @XmlTransient
    public List<ForexRates> getForexRatesList() {
        return forexRatesList;
    }

    public void setForexRatesList(List<ForexRates> forexRatesList) {
        this.forexRatesList = forexRatesList;
    }

    @XmlTransient
    public List<ForexRates> getForexRatesList1() {
        return forexRatesList1;
    }

    public void setForexRatesList1(List<ForexRates> forexRatesList1) {
        this.forexRatesList1 = forexRatesList1;
    }

    public Countries getCurrencyBaseCountry() {
        return currencyBaseCountry;
    }

    public void setCurrencyBaseCountry(Countries currencyBaseCountry) {
        this.currencyBaseCountry = currencyBaseCountry;
    }

    @XmlTransient
    public List<CommissionStructure> getCommissionStructureList() {
        return commissionStructureList;
    }

    public void setCommissionStructureList(List<CommissionStructure> commissionStructureList) {
        this.commissionStructureList = commissionStructureList;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    @XmlTransient
    public List<Transfer> getTransferList1() {
        return transferList1;
    }

    public void setTransferList1(List<Transfer> transferList1) {
        this.transferList1 = transferList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (currenciesID != null ? currenciesID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Currencies)) {
            return false;
        }
        Currencies other = (Currencies) object;
        if ((this.currenciesID == null && other.currenciesID != null) || (this.currenciesID != null && !this.currenciesID.equals(other.currenciesID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.Currencies[ currenciesID=" + currenciesID + " ]";
    }
    
}
