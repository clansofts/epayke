/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "HedgeSettings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HedgeSettings.findAll", query = "SELECT h FROM HedgeSettings h"),
    @NamedQuery(name = "HedgeSettings.findByHedgeSettingID", query = "SELECT h FROM HedgeSettings h WHERE h.hedgeSettingID = :hedgeSettingID"),
    @NamedQuery(name = "HedgeSettings.findByHedgeValue", query = "SELECT h FROM HedgeSettings h WHERE h.hedgeValue = :hedgeValue"),
    @NamedQuery(name = "HedgeSettings.findByHedgeApplicationDate", query = "SELECT h FROM HedgeSettings h WHERE h.hedgeApplicationDate = :hedgeApplicationDate"),
    @NamedQuery(name = "HedgeSettings.findByHedgeLoggDate", query = "SELECT h FROM HedgeSettings h WHERE h.hedgeLoggDate = :hedgeLoggDate")})
public class HedgeSettings implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "HedgeSettingID")
    private Integer hedgeSettingID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "HedgeValue")
    private Double hedgeValue;
    @Column(name = "HedgeApplicationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date hedgeApplicationDate;
    @Column(name = "HedgeLoggDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date hedgeLoggDate;
    @OneToMany(mappedBy = "hedgeComputation")
    private List<ForexRates> forexRatesList;

    public HedgeSettings() {
    }

    public HedgeSettings(Integer hedgeSettingID) {
        this.hedgeSettingID = hedgeSettingID;
    }

    public Integer getHedgeSettingID() {
        return hedgeSettingID;
    }

    public void setHedgeSettingID(Integer hedgeSettingID) {
        this.hedgeSettingID = hedgeSettingID;
    }

    public Double getHedgeValue() {
        return hedgeValue;
    }

    public void setHedgeValue(Double hedgeValue) {
        this.hedgeValue = hedgeValue;
    }

    public Date getHedgeApplicationDate() {
        return hedgeApplicationDate;
    }

    public void setHedgeApplicationDate(Date hedgeApplicationDate) {
        this.hedgeApplicationDate = hedgeApplicationDate;
    }

    public Date getHedgeLoggDate() {
        return hedgeLoggDate;
    }

    public void setHedgeLoggDate(Date hedgeLoggDate) {
        this.hedgeLoggDate = hedgeLoggDate;
    }

    @XmlTransient
    public List<ForexRates> getForexRatesList() {
        return forexRatesList;
    }

    public void setForexRatesList(List<ForexRates> forexRatesList) {
        this.forexRatesList = forexRatesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hedgeSettingID != null ? hedgeSettingID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HedgeSettings)) {
            return false;
        }
        HedgeSettings other = (HedgeSettings) object;
        if ((this.hedgeSettingID == null && other.hedgeSettingID != null) || (this.hedgeSettingID != null && !this.hedgeSettingID.equals(other.hedgeSettingID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.HedgeSettings[ hedgeSettingID=" + hedgeSettingID + " ]";
    }
    
}
