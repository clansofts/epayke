/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "RoleModulePermission")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RoleModulePermission.findAll", query = "SELECT r FROM RoleModulePermission r"),
    @NamedQuery(name = "RoleModulePermission.findByModulePermissionID", query = "SELECT r FROM RoleModulePermission r WHERE r.modulePermissionID = :modulePermissionID")})
public class RoleModulePermission implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ModulePermissionID")
    private Integer modulePermissionID;
    @JoinColumn(name = "ModuleInformation", referencedColumnName = "ModuleID")
    @ManyToOne
    private Modules moduleInformation;
    @JoinColumn(name = "RoleInfo", referencedColumnName = "RoleCenterID")
    @ManyToOne
    private RolesCenter roleInfo;

    public RoleModulePermission() {
    }

    public RoleModulePermission(Integer modulePermissionID) {
        this.modulePermissionID = modulePermissionID;
    }

    public Integer getModulePermissionID() {
        return modulePermissionID;
    }

    public void setModulePermissionID(Integer modulePermissionID) {
        this.modulePermissionID = modulePermissionID;
    }

    public Modules getModuleInformation() {
        return moduleInformation;
    }

    public void setModuleInformation(Modules moduleInformation) {
        this.moduleInformation = moduleInformation;
    }

    public RolesCenter getRoleInfo() {
        return roleInfo;
    }

    public void setRoleInfo(RolesCenter roleInfo) {
        this.roleInfo = roleInfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modulePermissionID != null ? modulePermissionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleModulePermission)) {
            return false;
        }
        RoleModulePermission other = (RoleModulePermission) object;
        if ((this.modulePermissionID == null && other.modulePermissionID != null) || (this.modulePermissionID != null && !this.modulePermissionID.equals(other.modulePermissionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.RoleModulePermission[ modulePermissionID=" + modulePermissionID + " ]";
    }
    
}
