/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "SubModules")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubModules.findAll", query = "SELECT s FROM SubModules s"),
    @NamedQuery(name = "SubModules.findBySubModuleID", query = "SELECT s FROM SubModules s WHERE s.subModuleID = :subModuleID"),
    @NamedQuery(name = "SubModules.findBySubModuleName", query = "SELECT s FROM SubModules s WHERE s.subModuleName = :subModuleName"),
    @NamedQuery(name = "SubModules.findBySubModuleLink", query = "SELECT s FROM SubModules s WHERE s.subModuleLink = :subModuleLink"),
    @NamedQuery(name = "SubModules.findBySubModulesIcon", query = "SELECT s FROM SubModules s WHERE s.subModulesIcon = :subModulesIcon"),
    @NamedQuery(name = "SubModules.findBySubModuleLogDate", query = "SELECT s FROM SubModules s WHERE s.subModuleLogDate = :subModuleLogDate")})
public class SubModules implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SubModuleID")
    private Integer subModuleID;
    @Size(max = 250)
    @Column(name = "SubModuleName")
    private String subModuleName;
    @Size(max = 100)
    @Column(name = "SubModuleLink")
    private String subModuleLink;
    @Size(max = 45)
    @Column(name = "SubModulesIcon")
    private String subModulesIcon;
    @Column(name = "SubModuleLogDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date subModuleLogDate;
    @JoinColumn(name = "ModuleDetails", referencedColumnName = "ModuleID")
    @ManyToOne
    private Modules moduleDetails;
    @OneToMany(mappedBy = "subModuleInfo")
    private List<RoleSubModulePermission> roleSubModulePermissionList;

    public SubModules() {
    }

    public SubModules(Integer subModuleID) {
        this.subModuleID = subModuleID;
    }

    public Integer getSubModuleID() {
        return subModuleID;
    }

    public void setSubModuleID(Integer subModuleID) {
        this.subModuleID = subModuleID;
    }

    public String getSubModuleName() {
        return subModuleName;
    }

    public void setSubModuleName(String subModuleName) {
        this.subModuleName = subModuleName;
    }

    public String getSubModuleLink() {
        return subModuleLink;
    }

    public void setSubModuleLink(String subModuleLink) {
        this.subModuleLink = subModuleLink;
    }

    public String getSubModulesIcon() {
        return subModulesIcon;
    }

    public void setSubModulesIcon(String subModulesIcon) {
        this.subModulesIcon = subModulesIcon;
    }

    public Date getSubModuleLogDate() {
        return subModuleLogDate;
    }

    public void setSubModuleLogDate(Date subModuleLogDate) {
        this.subModuleLogDate = subModuleLogDate;
    }

    public Modules getModuleDetails() {
        return moduleDetails;
    }

    public void setModuleDetails(Modules moduleDetails) {
        this.moduleDetails = moduleDetails;
    }

    @XmlTransient
    public List<RoleSubModulePermission> getRoleSubModulePermissionList() {
        return roleSubModulePermissionList;
    }

    public void setRoleSubModulePermissionList(List<RoleSubModulePermission> roleSubModulePermissionList) {
        this.roleSubModulePermissionList = roleSubModulePermissionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subModuleID != null ? subModuleID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubModules)) {
            return false;
        }
        SubModules other = (SubModules) object;
        if ((this.subModuleID == null && other.subModuleID != null) || (this.subModuleID != null && !this.subModuleID.equals(other.subModuleID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.SubModules[ subModuleID=" + subModuleID + " ]";
    }
    
}
