/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "Countries")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Countries.findAll", query = "SELECT c FROM Countries c"),
    @NamedQuery(name = "Countries.findByCountryID", query = "SELECT c FROM Countries c WHERE c.countryID = :countryID"),
    @NamedQuery(name = "Countries.findByCountyISO2Code", query = "SELECT c FROM Countries c WHERE c.countyISO2Code = :countyISO2Code"),
    @NamedQuery(name = "Countries.findByCountryBaseISOCurrency", query = "SELECT c FROM Countries c WHERE c.countryBaseISOCurrency = :countryBaseISOCurrency"),
    @NamedQuery(name = "Countries.findByCountryName", query = "SELECT c FROM Countries c WHERE c.countryName = :countryName"),
    @NamedQuery(name = "Countries.findByCountryNumber", query = "SELECT c FROM Countries c WHERE c.countryNumber = :countryNumber")})
public class Countries implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CountryID")
    private Integer countryID;
    @Size(max = 10)
    @Column(name = "CountyISO2Code")
    private String countyISO2Code;
    @Size(max = 10)
    @Column(name = "CountryBaseISOCurrency")
    private String countryBaseISOCurrency;
    @Size(max = 50)
    @Column(name = "CountryName")
    private String countryName;
    @Size(max = 10)
    @Column(name = "CountryNumber")
    private String countryNumber;
    @OneToMany(mappedBy = "currencyBaseCountry")
    private List<Currencies> currenciesList;
    @OneToMany(mappedBy = "country")
    private List<Receipients> receipientsList;
    @OneToMany(mappedBy = "nationality")
    private List<Customer> customerList;

    public Countries() {
    }

    public Countries(Integer countryID) {
        this.countryID = countryID;
    }

    public Integer getCountryID() {
        return countryID;
    }

    public void setCountryID(Integer countryID) {
        this.countryID = countryID;
    }

    public String getCountyISO2Code() {
        return countyISO2Code;
    }

    public void setCountyISO2Code(String countyISO2Code) {
        this.countyISO2Code = countyISO2Code;
    }

    public String getCountryBaseISOCurrency() {
        return countryBaseISOCurrency;
    }

    public void setCountryBaseISOCurrency(String countryBaseISOCurrency) {
        this.countryBaseISOCurrency = countryBaseISOCurrency;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryNumber() {
        return countryNumber;
    }

    public void setCountryNumber(String countryNumber) {
        this.countryNumber = countryNumber;
    }

    @XmlTransient
    public List<Currencies> getCurrenciesList() {
        return currenciesList;
    }

    public void setCurrenciesList(List<Currencies> currenciesList) {
        this.currenciesList = currenciesList;
    }

    @XmlTransient
    public List<Receipients> getReceipientsList() {
        return receipientsList;
    }

    public void setReceipientsList(List<Receipients> receipientsList) {
        this.receipientsList = receipientsList;
    }

    @XmlTransient
    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (countryID != null ? countryID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Countries)) {
            return false;
        }
        Countries other = (Countries) object;
        if ((this.countryID == null && other.countryID != null) || (this.countryID != null && !this.countryID.equals(other.countryID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.Countries[ countryID=" + countryID + " ]";
    }
    
}
