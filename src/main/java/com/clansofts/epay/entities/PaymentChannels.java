/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clansofts.epay.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author clansofts
 */
@Entity
@Table(name = "PaymentChannels")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentChannels.findAll", query = "SELECT p FROM PaymentChannels p"),
    @NamedQuery(name = "PaymentChannels.findByChannelID", query = "SELECT p FROM PaymentChannels p WHERE p.channelID = :channelID"),
    @NamedQuery(name = "PaymentChannels.findByChannelName", query = "SELECT p FROM PaymentChannels p WHERE p.channelName = :channelName")})
public class PaymentChannels implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ChannelID")
    private Integer channelID;
    @Size(max = 100)
    @Column(name = "ChannelName")
    private String channelName;
    @OneToMany(mappedBy = "paymentChannel")
    private List<Transfer> transferList;

    public PaymentChannels() {
    }

    public PaymentChannels(Integer channelID) {
        this.channelID = channelID;
    }

    public Integer getChannelID() {
        return channelID;
    }

    public void setChannelID(Integer channelID) {
        this.channelID = channelID;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (channelID != null ? channelID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentChannels)) {
            return false;
        }
        PaymentChannels other = (PaymentChannels) object;
        if ((this.channelID == null && other.channelID != null) || (this.channelID != null && !this.channelID.equals(other.channelID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.clansofts.epay.entities.PaymentChannels[ channelID=" + channelID + " ]";
    }
    
}
