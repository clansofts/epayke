-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 16, 2014 at 10:11 PM
-- Server version: 5.5.40-MariaDB-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `epay`
--

-- --------------------------------------------------------

--
-- Table structure for table `Card`
--

CREATE TABLE IF NOT EXISTS `Card` (
  `CardID` int(250) NOT NULL AUTO_INCREMENT,
  `AccountName` varchar(250) DEFAULT NULL,
  `CardNumber` varchar(100) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `CCV` varchar(3) DEFAULT NULL,
  `OwnerInfo` int(250) DEFAULT NULL,
  `CardType` int(11) DEFAULT NULL,
  PRIMARY KEY (`CardID`),
  KEY `OwnerInfoIDFK_idx` (`OwnerInfo`),
  KEY `CardTypeIDFK_idx` (`CardType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `CardTransactions`
--

CREATE TABLE IF NOT EXISTS `CardTransactions` (
  `CardDetailsID` int(250) NOT NULL AUTO_INCREMENT,
  `PaymentCard` int(250) DEFAULT NULL,
  `DeductedAmount` double(20,2) DEFAULT NULL,
  `TransactionDate` datetime DEFAULT NULL,
  `TransactionStamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`CardDetailsID`),
  KEY `PaymentCardIDFK_idx` (`PaymentCard`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `CardTypes`
--

CREATE TABLE IF NOT EXISTS `CardTypes` (
  `CardTypesID` int(11) NOT NULL AUTO_INCREMENT,
  `CardTypeName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CardTypesID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `CardTypes`
--

INSERT INTO `CardTypes` (`CardTypesID`, `CardTypeName`) VALUES
(1, 'Master Card'),
(2, 'Visa'),
(3, 'Euro');

-- --------------------------------------------------------

--
-- Table structure for table `CommissionStructure`
--

CREATE TABLE IF NOT EXISTS `CommissionStructure` (
  `StructureID` int(250) NOT NULL AUTO_INCREMENT,
  `SendBaseCurrency` int(11) DEFAULT NULL,
  `SendAmountFrom` double(20,2) DEFAULT NULL,
  `SendAmountTo` double(20,2) DEFAULT NULL,
  `CommissionType` int(11) DEFAULT NULL,
  `Commission` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`StructureID`),
  KEY `SendBaseCurrencyIDFK_idx` (`SendBaseCurrency`),
  KEY `CommissionTypeIDFK_idx` (`CommissionType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `CommissionTypes`
--

CREATE TABLE IF NOT EXISTS `CommissionTypes` (
  `TypeID` int(11) NOT NULL AUTO_INCREMENT,
  `TypeDesc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Countries`
--

CREATE TABLE IF NOT EXISTS `Countries` (
  `CountryID` int(11) NOT NULL AUTO_INCREMENT,
  `CountyISO2Code` varchar(10) DEFAULT NULL,
  `CountryBaseISOCurrency` varchar(10) DEFAULT NULL,
  `CountryName` varchar(50) DEFAULT NULL,
  `CountryNumber` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`CountryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Countries`
--

INSERT INTO `Countries` (`CountryID`, `CountyISO2Code`, `CountryBaseISOCurrency`, `CountryName`, `CountryNumber`) VALUES
(1, 'KE', 'KES', 'Kenya', '+254'),
(2, 'UG', 'UGX', 'Uganda', '+256'),
(3, 'US', 'USD', 'United States of America', '+1');

-- --------------------------------------------------------

--
-- Table structure for table `Currencies`
--

CREATE TABLE IF NOT EXISTS `Currencies` (
  `CurrenciesID` int(250) NOT NULL AUTO_INCREMENT,
  `CurrencyBaseCountry` int(11) DEFAULT NULL,
  `ISOCurrencyCode` varchar(10) DEFAULT NULL,
  `CurrencyName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CurrenciesID`),
  KEY `CurrencyCountryIDFK_idx` (`CurrencyBaseCountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE IF NOT EXISTS `Customer` (
  `CustomerID` int(250) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(250) DEFAULT NULL,
  `MiddleName` varchar(250) DEFAULT NULL,
  `OtherName` varchar(250) DEFAULT NULL,
  `Nationality` int(11) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `CountryState` varchar(100) DEFAULT NULL,
  `Address` varchar(250) DEFAULT NULL,
  `Email` varchar(250) DEFAULT NULL,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `ZipCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `CNationalityIDFK_idx` (`Nationality`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Employees`
--

CREATE TABLE IF NOT EXISTS `Employees` (
  `EmployeesID` int(250) NOT NULL,
  `EmployeeCode` varchar(45) DEFAULT NULL,
  `EmployeeName` varchar(250) DEFAULT NULL,
  `EmployeeEmailAddress` varchar(250) DEFAULT NULL,
  `EmployeePhoneNo` varchar(50) DEFAULT NULL,
  `EmployeeLogDate` varchar(45) DEFAULT NULL,
  `EmployeeOrganization` int(250) DEFAULT NULL,
  PRIMARY KEY (`EmployeesID`),
  KEY `EmployeeOrganizationIDFK_idx` (`EmployeeOrganization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ForexRates`
--

CREATE TABLE IF NOT EXISTS `ForexRates` (
  `ForexRatesID` int(250) NOT NULL AUTO_INCREMENT,
  `FromCurrency` int(250) DEFAULT NULL,
  `ToCurrency` int(250) DEFAULT NULL,
  `ExchangeRate` double(10,4) DEFAULT NULL,
  `HedgeRate` double(10,4) DEFAULT NULL,
  `Difference` double(10,4) DEFAULT NULL,
  `RateAppicationDate` date DEFAULT NULL,
  `LoggedBy` int(250) DEFAULT NULL,
  `LoggedOn` timestamp NULL DEFAULT NULL,
  `HedgeComputation` int(250) DEFAULT NULL,
  PRIMARY KEY (`ForexRatesID`),
  KEY `FromCurrencyIDFK_idx` (`FromCurrency`),
  KEY `ToCurrencyIDFK_idx` (`ToCurrency`),
  KEY `HedgeComputationIDFK_idx` (`HedgeComputation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `HedgeSettings`
--

CREATE TABLE IF NOT EXISTS `HedgeSettings` (
  `HedgeSettingID` int(250) NOT NULL AUTO_INCREMENT,
  `HedgeValue` double(10,4) DEFAULT NULL,
  `HedgeApplicationDate` datetime DEFAULT NULL,
  `HedgeLoggDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`HedgeSettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Modules`
--

CREATE TABLE IF NOT EXISTS `Modules` (
  `ModuleID` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(250) DEFAULT NULL,
  `ModuleLink` varchar(100) DEFAULT NULL,
  `ModuleIcon` varchar(45) DEFAULT NULL,
  `ModuleLogDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `OrganizationDetails`
--

CREATE TABLE IF NOT EXISTS `OrganizationDetails` (
  `OrganizationID` int(250) NOT NULL AUTO_INCREMENT,
  `OrganizationTypeInf` int(11) DEFAULT NULL,
  `OrganizationParent` int(250) DEFAULT NULL,
  `OrganizationCode` varchar(45) DEFAULT NULL,
  `OrganizationName` varchar(250) DEFAULT NULL,
  `Address` varchar(250) DEFAULT NULL,
  `PhoneNumber` varchar(45) DEFAULT NULL,
  `OpeningHours` varchar(45) DEFAULT NULL,
  `ClosingHour` varchar(45) DEFAULT NULL,
  `OrganizationStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`OrganizationID`),
  KEY `OrganizationTypeInfIDFK_idx` (`OrganizationTypeInf`),
  KEY `OrganizationStatusIDFK_idx` (`OrganizationStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `OrganizationStatus`
--

CREATE TABLE IF NOT EXISTS `OrganizationStatus` (
  `OrganizationStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `OrganizationStatusDesc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`OrganizationStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `OrganizationType`
--

CREATE TABLE IF NOT EXISTS `OrganizationType` (
  `OrganizationTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `OrganizationTypeDesc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`OrganizationTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `OrganizationType`
--

INSERT INTO `OrganizationType` (`OrganizationTypeID`, `OrganizationTypeDesc`) VALUES
(1, 'Head Office'),
(2, 'Branch'),
(3, 'Agent'),
(4, 'Money Transfer Organization - (MTO)'),
(5, 'Mobile Money Transfer - (MMT)');

-- --------------------------------------------------------

--
-- Table structure for table `PaymentChannels`
--

CREATE TABLE IF NOT EXISTS `PaymentChannels` (
  `ChannelID` int(50) NOT NULL AUTO_INCREMENT,
  `ChannelName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ChannelID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `PaymentChannels`
--

INSERT INTO `PaymentChannels` (`ChannelID`, `ChannelName`) VALUES
(1, 'Mobile - MPESA'),
(2, 'Mobile - Airtel Money'),
(3, 'Mobile - Orange Iko Pesa'),
(4, 'Mobile  - Yu Mobile');

-- --------------------------------------------------------

--
-- Table structure for table `Receipients`
--

CREATE TABLE IF NOT EXISTS `Receipients` (
  `ReceipientsID` int(250) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(250) DEFAULT NULL,
  `LastName` varchar(250) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Country` int(11) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `ZipCode` varchar(45) DEFAULT NULL,
  `MobileCarrier` varchar(100) DEFAULT NULL,
  `MobileNumber` varchar(50) DEFAULT NULL,
  `ReceipientOwner` int(250) DEFAULT NULL,
  `WalletInfo` int(250) DEFAULT NULL,
  PRIMARY KEY (`ReceipientsID`),
  KEY `RCountryIDFK_idx` (`Country`),
  KEY `ReceipientOwnerIDFK_idx` (`ReceipientOwner`),
  KEY `WalletInfoIDFK_idx` (`WalletInfo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ReceipientsWallet`
--

CREATE TABLE IF NOT EXISTS `ReceipientsWallet` (
  `WalletID` int(250) NOT NULL AUTO_INCREMENT,
  `MobileWallet` varchar(100) DEFAULT NULL,
  `NetworkCarrier` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`WalletID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RoleModulePermission`
--

CREATE TABLE IF NOT EXISTS `RoleModulePermission` (
  `ModulePermissionID` int(250) NOT NULL AUTO_INCREMENT,
  `RoleInfo` int(250) DEFAULT NULL,
  `ModuleInformation` int(11) DEFAULT NULL,
  PRIMARY KEY (`ModulePermissionID`),
  KEY `RoleInfoIDFK_idx` (`RoleInfo`),
  KEY `ModuleInformationIDFK_idx` (`ModuleInformation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RolesCenter`
--

CREATE TABLE IF NOT EXISTS `RolesCenter` (
  `RoleCenterID` int(250) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(100) DEFAULT NULL,
  `RoleOrganization` int(250) DEFAULT NULL,
  `RoleStatus` int(11) DEFAULT NULL,
  `RoleLogDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`RoleCenterID`),
  KEY `RoleOrganizationIDFK_idx` (`RoleOrganization`),
  KEY `RoleStatusIDFK_idx` (`RoleStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RolesStatus`
--

CREATE TABLE IF NOT EXISTS `RolesStatus` (
  `RoleStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `RolesStatusDesc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RoleStatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `RolesStatus`
--

INSERT INTO `RolesStatus` (`RoleStatusID`, `RolesStatusDesc`) VALUES
(1, 'Active'),
(2, 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `RoleSubModulePermission`
--

CREATE TABLE IF NOT EXISTS `RoleSubModulePermission` (
  `SubModulePermissionID` int(250) NOT NULL AUTO_INCREMENT,
  `RoleSMInfo` int(250) DEFAULT NULL,
  `ModuleSInfo` int(11) DEFAULT NULL,
  `SubModuleInfo` int(11) DEFAULT NULL,
  PRIMARY KEY (`SubModulePermissionID`),
  KEY `RoleSMInfoIDFK_idx` (`RoleSMInfo`),
  KEY `ModuleSInfoIDFK_idx` (`ModuleSInfo`),
  KEY `SubModuleInfoIDFK_idx` (`SubModuleInfo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SubModules`
--

CREATE TABLE IF NOT EXISTS `SubModules` (
  `SubModuleID` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleDetails` int(11) DEFAULT NULL,
  `SubModuleName` varchar(250) DEFAULT NULL,
  `SubModuleLink` varchar(100) DEFAULT NULL,
  `SubModulesIcon` varchar(45) DEFAULT NULL,
  `SubModuleLogDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`SubModuleID`),
  KEY `ModuleDetailsIDFK_idx` (`ModuleDetails`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Transfer`
--

CREATE TABLE IF NOT EXISTS `Transfer` (
  `TransferID` bigint(250) NOT NULL AUTO_INCREMENT,
  `TransferCode` varchar(100) NOT NULL,
  `Initiator` int(250) DEFAULT NULL,
  `Receipient` int(250) DEFAULT NULL,
  `SendCurrency` int(250) DEFAULT NULL,
  `SentAmount` double(20,2) DEFAULT NULL,
  `CommissionLevied` double(10,2) DEFAULT NULL,
  `TransferFxRate` int(250) DEFAULT NULL,
  `ReceipientCurrency` int(250) DEFAULT NULL,
  `ReceipientAmount` double(20,2) DEFAULT NULL,
  `TransferDate` datetime DEFAULT NULL,
  `TransferStatus` int(11) DEFAULT NULL,
  `PaymentChannel` int(11) DEFAULT NULL,
  `CardTransactionDets` int(250) DEFAULT NULL,
  PRIMARY KEY (`TransferID`),
  UNIQUE KEY `TransferCode_UNIQUE` (`TransferCode`),
  KEY `TransferStatusIDFK_idx` (`TransferStatus`),
  KEY `TransferFxRateIDFK_idx` (`TransferFxRate`),
  KEY `InitiatorIDFK_idx` (`Initiator`),
  KEY `ReceipientIDFK_idx` (`Receipient`),
  KEY `ReceipientCurrencyIDFK_idx` (`ReceipientCurrency`),
  KEY `SendCurrencyIDFK_idx` (`SendCurrency`),
  KEY `PaymentChannelIDFK_idx` (`PaymentChannel`),
  KEY `CardTransactionDetsIDFK_idx` (`CardTransactionDets`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TransfersLog`
--

CREATE TABLE IF NOT EXISTS `TransfersLog` (
  `TransfersLogID` bigint(250) NOT NULL,
  `TransferInfo` bigint(250) DEFAULT NULL,
  `LogDate` timestamp NULL DEFAULT NULL,
  `LogActivity` text,
  PRIMARY KEY (`TransfersLogID`),
  KEY `TransferInfoIDFK_idx` (`TransferInfo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `TransfersStatus`
--

CREATE TABLE IF NOT EXISTS `TransfersStatus` (
  `StatusID` int(11) NOT NULL AUTO_INCREMENT,
  `StatusName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `TransfersStatus`
--

INSERT INTO `TransfersStatus` (`StatusID`, `StatusName`) VALUES
(1, 'Initiated'),
(2, 'In Progress'),
(3, 'Paid'),
(4, 'Cancelled'),
(5, 'Suspended');

-- --------------------------------------------------------

--
-- Table structure for table `UsersActivities`
--

CREATE TABLE IF NOT EXISTS `UsersActivities` (
  `UsersActivitiesID` bigint(250) NOT NULL AUTO_INCREMENT,
  `UsersActivity` text,
  `UserInformation` int(250) DEFAULT NULL,
  `ActivityLogDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`UsersActivitiesID`),
  KEY `UserInformationIDFK_idx` (`UserInformation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UsersProfiles`
--

CREATE TABLE IF NOT EXISTS `UsersProfiles` (
  `UserProfileID` bigint(250) NOT NULL AUTO_INCREMENT,
  `UserType` int(11) DEFAULT NULL,
  `CustomerDets` int(250) DEFAULT NULL,
  `UserDetails` int(250) DEFAULT NULL,
  `RoleDets` int(100) DEFAULT NULL,
  `UserName` varchar(250) DEFAULT NULL,
  `PassCode` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`UserProfileID`),
  KEY `UserTypeIDFK_idx` (`UserType`),
  KEY `CustomerDetsIDFK_idx` (`CustomerDets`),
  KEY `UserDetailsIDFK_idx` (`UserDetails`),
  KEY `RoleDetsIDFK_idx` (`RoleDets`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UsersTypes`
--

CREATE TABLE IF NOT EXISTS `UsersTypes` (
  `UserTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `UsersTypeDesc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`UserTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `UsersTypes`
--

INSERT INTO `UsersTypes` (`UserTypeID`, `UsersTypeDesc`) VALUES
(1, 'System'),
(2, 'Agents'),
(3, 'Customers');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Card`
--
ALTER TABLE `Card`
  ADD CONSTRAINT `CardTypeIDFK` FOREIGN KEY (`CardType`) REFERENCES `CardTypes` (`CardTypesID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `OwnerInfoIDFK` FOREIGN KEY (`OwnerInfo`) REFERENCES `Customer` (`CustomerID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `CardTransactions`
--
ALTER TABLE `CardTransactions`
  ADD CONSTRAINT `PaymentCardIDFK` FOREIGN KEY (`PaymentCard`) REFERENCES `Card` (`CardID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `CommissionStructure`
--
ALTER TABLE `CommissionStructure`
  ADD CONSTRAINT `CommissionTypeIDFK` FOREIGN KEY (`CommissionType`) REFERENCES `CommissionTypes` (`TypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SendBaseCurrencyIDFK` FOREIGN KEY (`SendBaseCurrency`) REFERENCES `Currencies` (`CurrenciesID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Currencies`
--
ALTER TABLE `Currencies`
  ADD CONSTRAINT `CurrencyCountryIDFK` FOREIGN KEY (`CurrencyBaseCountry`) REFERENCES `Countries` (`CountryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Customer`
--
ALTER TABLE `Customer`
  ADD CONSTRAINT `CNationalityIDFK` FOREIGN KEY (`Nationality`) REFERENCES `Countries` (`CountryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Employees`
--
ALTER TABLE `Employees`
  ADD CONSTRAINT `EmployeeOrganizationIDFK` FOREIGN KEY (`EmployeeOrganization`) REFERENCES `OrganizationDetails` (`OrganizationID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ForexRates`
--
ALTER TABLE `ForexRates`
  ADD CONSTRAINT `FromCurrencyIDFK` FOREIGN KEY (`FromCurrency`) REFERENCES `Currencies` (`CurrenciesID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `HedgeComputationIDFK` FOREIGN KEY (`HedgeComputation`) REFERENCES `HedgeSettings` (`HedgeSettingID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ToCurrencyIDFK` FOREIGN KEY (`ToCurrency`) REFERENCES `Currencies` (`CurrenciesID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `OrganizationDetails`
--
ALTER TABLE `OrganizationDetails`
  ADD CONSTRAINT `OrganizationStatusIDFK` FOREIGN KEY (`OrganizationStatus`) REFERENCES `OrganizationStatus` (`OrganizationStatusID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `OrganizationTypeInfIDFK` FOREIGN KEY (`OrganizationTypeInf`) REFERENCES `OrganizationType` (`OrganizationTypeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Receipients`
--
ALTER TABLE `Receipients`
  ADD CONSTRAINT `RCountryIDFK` FOREIGN KEY (`Country`) REFERENCES `Countries` (`CountryID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ReceipientOwnerIDFK` FOREIGN KEY (`ReceipientOwner`) REFERENCES `Customer` (`CustomerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `WalletInfoIDFK` FOREIGN KEY (`WalletInfo`) REFERENCES `ReceipientsWallet` (`WalletID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `RoleModulePermission`
--
ALTER TABLE `RoleModulePermission`
  ADD CONSTRAINT `ModuleInformationIDFK` FOREIGN KEY (`ModuleInformation`) REFERENCES `Modules` (`ModuleID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RoleInfoIDFK` FOREIGN KEY (`RoleInfo`) REFERENCES `RolesCenter` (`RoleCenterID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `RolesCenter`
--
ALTER TABLE `RolesCenter`
  ADD CONSTRAINT `RoleOrganizationIDFK` FOREIGN KEY (`RoleOrganization`) REFERENCES `OrganizationDetails` (`OrganizationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RoleStatusIDFK` FOREIGN KEY (`RoleStatus`) REFERENCES `RolesStatus` (`RoleStatusID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `RoleSubModulePermission`
--
ALTER TABLE `RoleSubModulePermission`
  ADD CONSTRAINT `ModuleSInfoIDFK` FOREIGN KEY (`ModuleSInfo`) REFERENCES `Modules` (`ModuleID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RoleSMInfoIDFK` FOREIGN KEY (`RoleSMInfo`) REFERENCES `RolesCenter` (`RoleCenterID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SubModuleInfoIDFK` FOREIGN KEY (`SubModuleInfo`) REFERENCES `SubModules` (`SubModuleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `SubModules`
--
ALTER TABLE `SubModules`
  ADD CONSTRAINT `ModuleDetailsIDFK` FOREIGN KEY (`ModuleDetails`) REFERENCES `Modules` (`ModuleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Transfer`
--
ALTER TABLE `Transfer`
  ADD CONSTRAINT `CardTransactionDetsIDFK` FOREIGN KEY (`CardTransactionDets`) REFERENCES `CardTransactions` (`CardDetailsID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `InitiatorIDFK` FOREIGN KEY (`Initiator`) REFERENCES `Customer` (`CustomerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `PaymentChannelIDFK` FOREIGN KEY (`PaymentChannel`) REFERENCES `PaymentChannels` (`ChannelID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ReceipientCurrencyIDFK` FOREIGN KEY (`ReceipientCurrency`) REFERENCES `Currencies` (`CurrenciesID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ReceipientIDFK` FOREIGN KEY (`Receipient`) REFERENCES `Receipients` (`ReceipientsID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SendCurrencyIDFK` FOREIGN KEY (`SendCurrency`) REFERENCES `Currencies` (`CurrenciesID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TransferFxRateIDFK` FOREIGN KEY (`TransferFxRate`) REFERENCES `ForexRates` (`ForexRatesID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TransferStatusIDFK` FOREIGN KEY (`TransferStatus`) REFERENCES `TransfersStatus` (`StatusID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `TransfersLog`
--
ALTER TABLE `TransfersLog`
  ADD CONSTRAINT `TransferInfoIDFK` FOREIGN KEY (`TransferInfo`) REFERENCES `Transfer` (`TransferID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `UsersActivities`
--
ALTER TABLE `UsersActivities`
  ADD CONSTRAINT `UserInformationIDFK` FOREIGN KEY (`UserInformation`) REFERENCES `UsersProfiles` (`UserType`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `UsersProfiles`
--
ALTER TABLE `UsersProfiles`
  ADD CONSTRAINT `CustomerDetsIDFK` FOREIGN KEY (`CustomerDets`) REFERENCES `Customer` (`CustomerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RoleDetsIDFK` FOREIGN KEY (`RoleDets`) REFERENCES `RolesCenter` (`RoleCenterID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `UserDetailsIDFK` FOREIGN KEY (`UserDetails`) REFERENCES `Employees` (`EmployeesID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `UserTypeIDFK` FOREIGN KEY (`UserType`) REFERENCES `UsersTypes` (`UserTypeID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
